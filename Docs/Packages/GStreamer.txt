**********************************************************************
* GSTREAMER
**********************************************************************



======================================================================
= Depencies
======================================================================
[Required]
    - GLib

[Optional]
    - pango


======================================================================
= Unix / Mac OSX
======================================================================
git clone git://anongit.freedesktop.org/gstreamer/cerbero [Path]
cd [Path]
./cerbero-uninstallled bootstrap

# 2018-08-15
# May need to manually download libxml2, due to the address points to
# another library.

# On OSX
# May need to skip libxml2


--------------------------------
# NB! Pkg-config should at least be pointing to GLib and GStreamer.
export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:[SdkPath]


--------------------
- GStreamer
--------------------
git clone https://github.com/GStreamer/gstreamer [Path]

cd [Path]
git pull
git checkout tags/[Tags] -b [Version]

./autogen.sh --prefix=[SDK_Path] --disable-gtk-doc --disable-examples --disable-tests --disable-benchmarks
make install


--------------------
- Plugin-Base/Good/Bad
--------------------
git clone https://github.com/GStreamer/gst-plugins-base.git [Path]
git clone https://github.com/GStreamer/gst-plugins-bad.git [Path]
git clone https://github.com/GStreamer/gst-plugins-good.git [Path]

cd [Path]
git pull
git checkout tags/[Tags] -b [Version]

# On OSX add following for gstplugin-base
# --enable-glx=no
./autogen.sh --prefix=[SDK_Path] --disable-gtk-doc --disable-examples --disable-tests --disable-benchmarks
make install


