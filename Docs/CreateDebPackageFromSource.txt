**********************************************************************
* Ubuntu 14.04 LTS x86_64
**********************************************************************


======================================================================
= Setup Development Environment
======================================================================
# Required:
sudo apt-get install build-essential autoconf automake libtool pkg-config autotools-dev debhelper fakeroot lintian dh-make

# Optional:
sudo apt-get install pbuilder debootstrap devsripts ubuntu-dev-tools


# Setup Maintainerinfo.
export DEBFULLNAME=<UserName>
export DEBEMAIL=<EMail>


# Optional:
# Change pbuilder folder (if wanted).
export PBUILDFOLER=~/Builds/pbuilder

# Create an clean ubuntu environment for packaging.
pbuilder-dist trusty create


======================================================================
= zlib
======================================================================
wget http://zlib.net/zlib-1.2.8.tar.gz
tar vxzf <DownloadedFile>
cd <ExtractedFolder>
dh_make --createorig --library --copyright mit --packagename <PackageName> --indep

# ex:
dh_make --createorig --library --copyright mit --packagename sdk-zlib --indep

# Optional:
cd debian
rm *.ex *.EX README* docs


Make neccessary changes in changelog and copyright.


----------------------------------------------------------------------
- debian/control
----------------------------------------------------------------------
Source: sdk-zlib
Priority: optional
Maintainer: <UserName> <<EMail>>
Build-Depends: debhelper (>= 8.0.0)
Standards-Version: 3.9.4
Section: libs
Homepage: http://zlib.net

Package: sdk-zlib
Section: libs
Architecture: any
Conflicts: zlib1g-dev (= ${binary:Version})
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Compression library.
 Part of SDK.

# Optional:
Package: sdk-zlib-dev
Section: libdevel
Architecture: any
Depends: sdk-zlib (= ${binary:Version}), ${misc:Depends}
Description: Compression library (header development).
 Part of SDK.

Package: sdk-zlib-static
Section: libdevel
Architecture: any
Depends: sdk-zlib-dev
Description: Compression library (static library).
 Part of SDK.
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/rules
----------------------------------------------------------------------
#!/usr/bin/make -f
# output every command that modifies files on the build system.
export DH_VERBOSE = 1

build:
	dh_testdir

	./configure --prefix=/usr

	dh_auto_build
	dh_auto_test


build-static:
	dh_testdir

	./configure --prefix=/usr --static

	dh_auto_build
	dh_auto_test

%:
	dh $@ 


override_dh_auto_clean:
	$(MAKE) distclean
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib.dirs
----------------------------------------------------------------------
usr/lib
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib-dev.dirs
----------------------------------------------------------------------
usr/include
usr/lib
usr/lib/pkgconfig
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib-static.dirs
----------------------------------------------------------------------
usr/lib
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib.install
----------------------------------------------------------------------
usr/lib/*.so.*
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib-dev.install
----------------------------------------------------------------------
usr/include/*
usr/lib/pkgconfig/*
----------------------------------------------------------------------


----------------------------------------------------------------------
- debian/sdk-zlib-static.install
----------------------------------------------------------------------
usr/lib/*lib*.a
----------------------------------------------------------------------


# -uc => unsigned change log.
# -us => unsigned source file (.dsc).
# -tc => clean generated files from build directory under <SourceDolder/debian>.
dpkg-buildpackage -uc -us -tc -rfakeroot

# Optional:
# Test with clean environment.
sudo pbuilder --build --distribution trusty --architecture amd64 --basetgz ~/Builds/pbuilder/trusty-base.gtz ./sdk-zlib_1.2.8-1.dsc

