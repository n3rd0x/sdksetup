# ----------------------------------------
# SDK
# ----------------------------------------
# Generic routine to build a SDK.
python3 sdkBuild.py --source-dir /home/ubuntu/Devs/Sources --build-dir /home/ubuntu/Devs/Builds --sdk-dir /home/ubuntu/Devs/SDK/Augmenti --suffix gcc4.8.4-jetson-tk1 --build-all 1 --create-sdk 1

# Build a specific package, for instance; OGRE
python3 sdkBuild.py --source-dir /home/ubuntu/Devs/Sources --build-dir /home/ubuntu/Devs/Builds --sdk-dir /home/ubuntu/Devs/SDK/Augmenti --suffix gcc4.8.4-jetson-tk1 --build-all 0 --create-sdk 1 --build-ogre 1

# Create packages, for instance; a deb-package.
python3 sdkBuild.py --source-dir /home/ubuntu/Devs/Sources --build-dir /home/ubuntu/Devs/Builds --sdk-dir /home/ubuntu/Devs/SDK/Augmenti --suffix gcc4.8.4-jetson-tk1 --build-all 1 --create-packages 1 --package-maintainer "T.Sang Tran" --package-maintainer-email "sang@augmenti.no" --package-arch amd64 --package-prefix augmenti


# ----------------------------------------
# GTK (Microsoft Windows)
# ----------------------------------------
# Build GTK on windows.
# Please follow description on https://github.com/wingtk/gtk-win32 before using this script.
python sdkBuildGtk.py --suffix msvc2013-x86 --build-dir c:/Devs/Builds --msys-dir c:/Bin/MSYS --sdk-dir c:/Devs/SDK/Augmenti --source-dir c:/Devs/Sources --revision 0dc602621fa113ab55e1e402738022535139cb14


# ----------------------------------------
# Microsoft Visual Studio
# ----------------------------------------
# On Windows we may use the vcpkg.
https://github.com/microsoft/vcpkg