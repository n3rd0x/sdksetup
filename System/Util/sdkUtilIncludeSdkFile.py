# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
import subprocess
from System import sdkRoot
from System.Util.sdkUtilCore import generateTimestamp
from System.Util.sdkUtilCore import isExists
from System.Util.sdkUtilLog import logMessage


# ------------------------------------------------------------
# - SDK CMake (IncludeSdk.cmake)
# ------------------------------------------------------------
class IncludeSdkFile:
    # Initialisation.
    def __init__(self):
        self.lines = []
        self.lines.append("# ************************************************************")
        self.lines.append("# SDK CMAKE Configuration")
        self.lines.append("# Generated " + generateTimestamp())
        self.lines.append("# ************************************************************")
        for l in self._generateInfo(True):
            self.lines.append(l)
        self.lines.append("# Auto generated CMAKE variable below.")

        customFile = os.path.join(sdkRoot.InfoPath.mSdkRoot, "IncludeCustom.cmake")
        if not isExists(customFile):
            file = open(customFile, "w")
            file.write("# ************************************************************\r\n")
            file.write("# Custom SDK CMAKE Configuration\r\n")
            file.write("# ************************************************************\r\n")
            lines = self._generateInfo(False)
            for l in lines:
                file.write(l)
                file.write("\r\n")
            file.write("# Custom defined CMAKE variable below.\r\n")
            file.write("\r\n")
            file.close()
            logMessage("Creating custom SDK CMAKE file [" + customFile + "].")


    # Generate info.
    def _generateInfo(self, isSdkFile):
        lines = []
        lines.append("# Macro for setting CMake variable.")
        lines.append("if(NOT COMMAND set_sdk_library)")
        lines.append("  macro(SET_SDK_LIBRARY PREFIX PATH)")
        lines.append("      if(DEFINED ${PREFIX})")
        lines.append("          # Force setting the package's home to SDK home path, only if the variable is empty.")
        lines.append("          if(NOT ${PREFIX})")
        lines.append("              set(${PREFIX} \"${PROJECT_PATH_SDK_HOME}/${PATH}\" CACHE PATH \"Path to ${PREFIX}.\" FORCE)")
        lines.append("          endif()")
        lines.append("      endif()")
        lines.append("  endmacro()")
        lines.append("endif()")
        lines.append("if(NOT COMMAND set_path_library)")
        lines.append("  macro(SET_PATH_LIBRARY PREFIX PATH)")
        lines.append("      if(DEFINED ${PREFIX})")
        lines.append("          if(NOT ${PREFIX})")
        lines.append("              set(${PREFIX} \"${PATH}\" CACHE PATH \"Path to ${PREFIX}.\" FORCE)")
        lines.append("          endif()")
        lines.append("      endif()")
        lines.append("  endmacro()")
        lines.append("endif()")
        lines.append("")
        if isSdkFile:
            lines.append("# NB! This is generated from the script (sdkBuild.py). Manual changes on this file")
            lines.append("#     may be lost, if sdkBuild.py is called.")
            lines.append("#")
        lines.append("# Example:")
        lines.append("# set_sdk_library(ZLIB_HOME \"zlib/1.2.8-gcc4.8.4-x64\")")
        lines.append("")
        return lines


    # Add package.
    def add(self, prefix, name, version):
        if prefix:
            # set_sdk_library(MYLIB "mylib/1.0-allvm-9.0-x86_64")
            self.lines.append(
                "set_sdk_library("      +
                prefix                  + " \"" +
                name                    + "/"   +
                version                 + "-"   +
                sdkRoot.InfoPath.mSuffix   + "\")"
            )


    # Add package as full path.
    def addFullPath(self, prefix, path):
        if prefix:
            # set_path_library(MYLIB "/path2/mylib/")
            self.lines.append(
                "set_path_library("      +
                prefix                  + " \"" +
                path                    + "\")"
            )


    # Save.
    def save(self):
        sdkFile = os.path.join(sdkRoot.InfoPath.mSdkRoot, "IncludeSdk.cmake")
        file = open(sdkFile, "w")
        for line in self.lines:
            file.write(line)
            file.write("\r\n")
        file.write("\r\n\r\n")
        file.close()
        logMessage("Creating SDK CMAKE file [" + sdkFile + "].")
