# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
import subprocess

# Local import.
from System import sdkRoot
from System.Util.sdkUtilLog import *
from System.Util.sdkUtilString import toString


# ------------------------------------------------------------
# - Wget
# ------------------------------------------------------------
class Wget:
    # Initialisation.
    def __init__(self, fileName , url):
        self.mFileName  = fileName
        self.mUrl       = url


    # Download.
    def download(self, path):
        file = os.path.join(path, self.mFileName)
        logDownloading(self.mFileName)
        if not isExists(file):
            logMessage("Download from [" + self.mUrl + "] into [" + path + "].")
            process  = subprocess.Popen(["wget", "--directory-prefix", path, self.mUrl])
            process.wait()
            if process.returncode >= 1:
                logDownloading(self.mFileName, "failed")
                return (False, file)
        else:
            logMessage("File [" + file + "] already exists. ** SKIP ** downloading process.")

        logDownloading(self.mFileName, "completed")
        return (True, file)