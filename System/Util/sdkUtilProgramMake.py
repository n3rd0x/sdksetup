# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCompiler import *
from System.Util.sdkUtilProcess import runProcess


# ------------------------------------------------------------
# - Make
# ------------------------------------------------------------
class Make:
    # Initialisation.
    def __init__(self, compiler = ""):
        self.mCompiler = compiler
        if not self.mCompiler:
            self.mCompiler = Compiler()
        if self.mCompiler.isUnknown():
            self.mCompiler.setDefault()
        self._setMake(self.mCompiler.getCompiler())


    # Set make program.
    def _setMake(self, compiler):
        if compiler == COMPILER_MSVC_NMAKE:
            self.mMake = "nmake"
        else:
            self.mMake = "make"


    # Clean
    def clean(self, arg = "distclean"):
        return runProcess([self.mMake, arg], "MakeClean")


    # Compile.
    def compile(self, args = []):
        make = [self.mMake]
        if sdkRoot.Platform.isUnixAlike():
            make.append("--jobs=4")
        for arg in args:
            make.append(arg)

        return runProcess(make, "Make", "sdkMake.log")


    # Install.
    def install(self):
        return runProcess([self.mMake, "install"], "MakeInstall")

