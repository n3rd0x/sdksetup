# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import platform
import subprocess
from System.Util.sdkUtilString import toString



# ------------------------------------------------------------
# - Global Enumeration
# ------------------------------------------------------------
# Machine.
MACHINE_UNKNOWN = 0
MACHINE_X86_64 = 1
MACHINE_X32 = 2
MACHINE_X64 = 3
MACHINE_AMD64 = 4

# Platform.
PLATFORM_UNKNOWN    = 0
PLATFORM_UNIX       = 1
PLATFORM_WINDOWS    = 2
PLATFORM_DARWIN     = 3

# Processor.
PROCESSOR_UNKNOWN = 0
PROCESSOR_I386 = 1

# System.
SYSTEM_UNKNOWN      = 0
SYSTEM_UBUNTU       = 1
SYSTEM_WINDOWS      = 2
SYSTEM_MAC_OSX      = 3




# ------------------------------------------------------------
# - Check Platform & OS
# ------------------------------------------------------------
class Platform:
    mMachine = [MACHINE_UNKNOWN, "Unknown"]
    mPlatform = [PLATFORM_UNKNOWN, "Unknown"]
    mProcessor = [PROCESSOR_UNKNOWN, "Unknown"]
    mSystem = [SYSTEM_UNKNOWN, "Unknown"]
    mVersion = ""

    # Check platform on initialisation.
    def __init__(self):
        # ====================
        self.checkPlatform()


    # Check system.
    def checkPlatform(self):
        # ====================
        # Default
        # ====================
        self.mMachine = [MACHINE_UNKNOWN, "Unknown"]
        self.mPlatform = [PLATFORM_UNKNOWN, "Unknown"]
        self.mProcessor = [PROCESSOR_UNKNOWN, "Unknown"]
        self.mSystem = [SYSTEM_UNKNOWN, "Unknown"]
        self.mVersion = ""


        # ====================
        # Debug
        # ====================
        print("[DEBUG] Machine:", platform.machine())
        print("[DEBUG] Platform:", platform.system())
        print("[DEBUG] Processor:", platform.processor())


        # ====================
        # Processor
        # ====================
        if platform.machine() == "x86_64":
            self.mMachine = [MACHINE_X86_64, "x86_64"]


        # ====================
        # Platform
        # ====================
        if platform.system() == "Darwin":
            self.mPlatform = [PLATFORM_DARWIN, "Darwin"]


        # ====================
        # Processor
        # ====================
        if platform.processor() == "i386":
            self.mProcessor = [PROCESSOR_I386, "i386"]


        # ====================
        # System
        # ====================
        self.checkSystem()


    # Check system.
    def checkSystem(self):
        if self.isDarwin():
            self.mSystem = [SYSTEM_MAC_OSX, "Mac OSX"]
            self.mVersion, version, machine = platform.mac_ver()
        elif self.isUnix():
            process  = subprocess.Popen(["lsb_release", "-i", "-s"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = process.communicate()
            if len(out) > 0:
                sys = out.decode(encoding="UTF-8")
                sys = sys[:len(sys) - 1]
                if sys == "Ubuntu":
                    process  = subprocess.Popen(["lsb_release", "-r", "-s"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    out, err = process.communicate()
                    ver = out.decode(encoding="UTF-8")
                    ver = ver[:len(ver) - 1]
                    self.mSystem = [SYSTEM_UBUNTU, "Ubuntu"]
                    self.mVersion = ver


    # Return Darwin platform.
    def isDarwin(self):
        return self.mPlatform[0] == PLATFORM_DARWIN


    # Return Unix platform.
    def isUnix(self):
        return self.mPlatform[0] == PLATFORM_UNIX


    # Return Unix alike platform.
    def isUnixAlike(self):
        return (self.mPlatform[0] == PLATFORM_UNIX or self.mPlatform[0] == PLATFORM_DARWIN)


    # Return Windows platform.
    def isWindows(self):
        return self.mPlatform[0] == PLATFORM_WINDOWS


    # Return Mac OSX system.
    def isMacOsx(self):
        return self.mSystem[0] == SYSTEM_MAC_OSX


    # Return Ubuntu system.
    def isUbuntu(self):
        return self.mSystem[0] == SYSTEM_UBUNTU


    # Return unknown system.
    def isUnknownSystem(self):
        return self.mSystem[0] == SYSTEM_UNKNOWN


    # Return machine X86_64
    def isMachine_x86_64(self):
        return self.mMachine[0] == MACHINE_X86_64


    # Print
    def print(self):
        for i in self.toString():
            print(toString(i))


    # To string.
    def toString(self):
        msg = []
        msg.append("----------------------------------------")
        msg.append("- PLATFORM INFO")
        msg.append("----------------------------------------")
        msg.append("Platform:  " + self.mPlatform[1])
        msg.append("System:    " + self.mSystem[1])
        if self.mVersion != "":
            msg.append("Version:   " + self.mVersion)
        msg.append("Machine:   " + self.mMachine[1])
        msg.append("Processor: " + self.mProcessor[1])
        msg.append("----------------------------------------")
        return msg

