# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
from System import sdkRoot
from System.Util.sdkUtilString import toString
from System.Util.sdkUtilCore import isExists




# ------------------------------------------------------------
# - Log Definitions
# ------------------------------------------------------------
def logMessage(msg, output = True):
    sdkRoot.Logger.log(msg, output)


def logging(start, var, end = ""):
    logMessage((start, var + "....", end))


def logBuilding(var, end = ""):
    logging("Building", var, end)


def logConfigure(var, end = ""):
    logging("Configuring", var, end)


def logCompiling(var, end = ""):
    logging("Compiling", var, end)


def logCopying(var, end = ""):
    logging("Copying", var, end)


def logCloning(var, end = ""):
    logging("Cloning", var, end)


def logDownloading(var, end = ""):
    logging("Downloading", var, end)


def logExtracting(var, end = ""):
    logging("Extracting", var, end)


def logLooking(var, end = ""):
    logging("Looking for", var, end)




# ------------------------------------------------------------
# - Logger
# ------------------------------------------------------------
class LogFile:
    # Initialisation a file for logging.
    def __init__(self, path, fileName):
        self.mFileName = os.path.join(path, fileName)

        # Remove existing file.
        if isExists(self.mFileName):
            os.remove(self.mFileName)
        self.mFile = open(self.mFileName, 'w')


    # Close.
    def close(self):
        self.mFile.close()


    # Log.
    def log(self, msg, output = True):
        if type(msg) is list:
            for m in msg:
                self.log(m, output)
        else:
            out = toString(msg)
            if out:
                if output:
                    print(out)
                self.mFile.write(out)
                if sdkRoot.Platform.isWindows():
                    self.mFile.write("\n")
                else:
                    self.mFile.write("\r\n")

