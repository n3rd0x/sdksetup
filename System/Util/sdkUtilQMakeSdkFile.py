# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
import subprocess
from System import sdkRoot
from System.Util.sdkUtilCore import generateTimestamp
from System.Util.sdkUtilLog import logMessage


# ------------------------------------------------------------
# - QMake File
# ------------------------------------------------------------
class QMakeSdkFile:
    # Initialisation.
    def __init__(self):
        self.lines = []
        self.lines.append("# ************************************************************")
        self.lines.append("# SDK Package Information for QMake")
        self.lines.append("# Generated " + generateTimestamp())
        self.lines.append("# ************************************************************")
        self.lines.append("")


    # Add package.
    def add(self, pkg, useGeneric):
        #self.lines.append("# ********************")
        #self.lines.append("# " + pkg.mName)
        #self.lines.append("# ********************")
        if useGeneric:
            if not (pkg.mPrecompiled or pkg.mSystemUsage):
                pkgName = "$${SDK_PATH}/" + pkg.mPrefix + "/" + pkg.mVersion + "-$${SDK_SUFFIX}"
                self.lines.append("INCLUDEPATH += " + pkgName + "/include")
                self.lines.append("LIBS += -L" + pkgName + "/lib/release -l" + pkg.mNameRelease)
            else:
                self.lines.append("INCLUDEPATH += " + pkg.getInclude())
                self.lines.append("LIBS += -L" + pkg.getPathRelease() + " -l" + pkg.mNameRelease)
        else:
            self.lines.append("INCLUDEPATH += " + pkg.getInclude())
            self.lines.append("LIBS += -L" + pkg.getPathRelease() + " -l" + pkg.mNameRelease)
        #self.lines.append("# ********************")
        #self.lines.append("")


    # Save.
    def save(self):
        sdkFile = os.path.join(sdkRoot.InfoPath.mSdkRoot, "QMakeSdkFile.txt")
        file = open(sdkFile, "w")
        for line in self.lines:
            file.write(line)
            file.write("\r\n")
        file.close()
        logMessage("Creating QMake SDK file [" + sdkFile + "].")

