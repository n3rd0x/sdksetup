# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot



# ------------------------------------------------------------
# - Global Enumeration
# ------------------------------------------------------------
# Compiler
COMPILER_UNKNOWN        = 0
COMPILER_GNU_GCC        = 1
COMPILER_CLANG          = 2
COMPILER_CLANG_APPLE    = 3
COMPILER_XCODE          = 4
COMPILER_MSVC           = 5
COMPILER_MSVC_NMAKE     = 6


# ------------------------------------------------------------
# - Compiler
# ------------------------------------------------------------
class Compiler:
    # Initialisation
    def __init__(self, compiler = COMPILER_UNKNOWN, version = ""):
        self.mCompiler = [COMPILER_UNKNOWN, "Unknown"]
        self.mVersion  = ""
        self.setDefault()


    # Check compiler.
    def _checkCompiler(self, compiler = COMPILER_UNKNOWN, version = ""):
        self.mCompiler[0]   = compiler
        self.mVersion       = version
        if self.mCompiler[0] == COMPILER_GNU_GCC:
            self.mCompiler[1] = "GNU GCC"
        elif self.mCompiler[0] == COMPILER_CLANG:
            self.mCompiler[1] = "Clang"
        elif self.mCompiler[0] == COMPILER_CLANG_APPLE:
            self.mCompiler[1] = "Apple Clang"
        elif self.mCompiler[0] == COMPILER_XCODE:
            self.mCompiler[1] = "XCode"
        elif self.mCompiler[0] == COMPILER_MSVC:
            self.mCompiler[1] = "Visual Studio"
        elif self.mCompiler[0] == COMPILER_MSVC_NMAKE:
            self.mCompiler[1] = "Visual Studio (NMake)"
        else:
            self.mCompiler[0] = COMPILER_UNKNOWN
            self.mCompiler[1] = "UNKNOWN"


    # Return compiler.
    def getCompiler(self):
        return self.mCompiler[0]


    # Return compiler name.
    def getCompilerName(self):
        return self.mCompiler[1]


    # Return version.
    def getVersion(self):
        return self.mVersion


    # Return Clang.
    def isClang(self):
        return self.mCompiler[0] == COMPILER_CLANG


    # Return Clang alike.
    def isClangAlike(self):
        return (
            self.isClang() or
            self.isXcode()
        )


    # Return GCC.
    def isGnuGcc(self):
        return self.mCompiler[0] == COMPILER_GCC


    # Return GCC alike.
    def isGnuGccAlike(self):
        return (
            self.isGnuGcc() or
            self.isClang()
        )


    # Return Visual Studio.
    def isMsvc(self):
        return self.mCompiler[0] == COMPILER_MSVC


    # Return Visual Studio (nmake).
    def isMsvcNmake(self):
        return self.mCompiler[0] == COMPILER_MSVC_NMAKE


    # Return Xcode.
    def isXcode(self):
        return self.mCompiler[0] == COMPILER_XCODE


    # Return unknown.
    def isUnknown(self):
        return self.mCompiler[0] == COMPILER_UNKNOWN


    # Print.
    def printInfo(self):
        for i in self.toStringInfo():
            print(toString(i))


    # Set data.
    def setCompiler(self, compiler = COMPILER_UNKNOWN, version = ""):
        self._checkCompiler(compiler, version)


    # Set default for platform specific.
    def setDefault(self):
        if sdkRoot.Platform.isMacOsx():
            self.setCompiler(COMPILER_XCODE)
        elif sdkRoot.Platform.isWindows():
            self.setCompiler(COMPILER_MSVC_NMAKE)
        else:
            self.setCompiler(COMPILER_GNU_GCC)


    # To string.
    def toString(self):
        msg = self.mCompiler[1]
        if self.mVersion != "":
            msg += " (" + self.mVersion + ")"
        return msg


    # To string.
    def toStringInfo(self):
        msg = []
        msg.append("----------------------------------------")
        msg.append("- COMPILER INFO")
        msg.append("----------------------------------------")
        msg.append("Compiler:  " + self.mCompiler[1])
        if self.mVersion != "":
            msg.append("Version:   " + self.mVersion)
        msg.append("----------------------------------------")
        return msg
