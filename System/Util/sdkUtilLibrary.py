# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import copy
import os
import pathlib
import shutil
import tarfile
from System import sdkRoot
from System.Util import *


# ------------------------------------------------------------
# - Helper Class
# ------------------------------------------------------------
class PathData:
    # Initialisation.
    def __init__(self):
        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64
        self.mSdkRoot           = ""

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/include
        self.mIncludeRoot       = ""
        self.mInclude           = ""

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/lib
        self.mLibraryRoot       = ""
        self.mLibraryDebug      = ""
        self.mLibraryRelease    = ""

        # Full file path.
        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/lib/libmyLib_d.so
        self.mFileDebug         = ""
        self.mFileRelease       = ""



# ------------------------------------------------------------
# - Generic Library
# - Contains base information of the library.
# ------------------------------------------------------------
class Library:
    # Global list of library.
    mLibraries = []

    # Initialisation.
    def __init__(self, name, prefix, version, url, revision = "", website = "", desc = ""):
        # ====================
        # Core
        # ====================
        self.mName              = name
        self.mPrefix            = prefix
        self.mVersion           = version
        self.mUrl               = url
        self.mRevision          = revision
        self.mWebsite           = website
        self.mDesc              = desc
        self.mPathInfo          = PathData()
        self.mPathSystem        = PathData()

        # Flag to indicate wether the info is set or not.
        self.mSystemInfo = False

        # Flag for using system libraries.
        self.mSystemUsage = False

        # Flag to indicate that the library is precompiled or not.
        self.mPrecompiled = False


        # ====================
        # Paths
        # ====================
        # Ex: 1.0-allvm-9.0-x86_64
        self.mVersionSuffix         = version + "-" + sdkRoot.InfoPath.mSuffix

        # Ex: /Builds/myLib/1.0-allvm-9.0-x86_64
        self.mPathBuild             = os.path.join(sdkRoot.InfoPath.mBuild, prefix, self.mVersionSuffix)

        # Ex: /Sources/myLib
        self.mPathSourceRoot        = os.path.join(sdkRoot.InfoPath.mSource, prefix)

        # Ex: /Sources/myLib/1.0
        self.mPathSource            = os.path.join(self.mPathSourceRoot, version)

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib
        self.mPathSdkRoot           = os.path.join(sdkRoot.InfoPath.mSdkLib, prefix)

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64
        self.mPathInfo.mSdkRoot     = os.path.join(self.mPathSdkRoot, self.mVersionSuffix)

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/include
        self.mPathInfo.mIncludeRoot       = os.path.join(self.mPathInfo.mSdkRoot, "include")
        self.mPathInfo.mInclude           = self.mPathInfo.mIncludeRoot

        # Ex: /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/lib
        self.mPathInfo.mLibraryRoot       = os.path.join(self.mPathInfo.mSdkRoot, "lib")
        self.mPathInfo.mLibraryDebug      = os.path.join(self.mPathInfo.mLibraryRoot, sdkRoot.InfoPath.mPrefixDebug)
        self.mPathInfo.mLibraryRelease    = os.path.join(self.mPathInfo.mLibraryRoot, sdkRoot.InfoPath.mPrefixRelease)


        # ====================
        # Names
        # ====================
        # Generic name (myLib_d / myLib)
        self.mNameDebug                 = ""
        self.mNameRelease               = ""

        # Common library name (mylib")
        self.mLibraryName               = ""

        # Full library name (libmyLib_d.so / libmyLib.so)
        self.mLibraryNameDebug          = ""
        self.mLibraryNameRelease        = ""


        # ====================
        # Install Options
        # ====================
        # For each build type, we will copy the specified directory.
        # Structure:
        # [Opt, Name]
        # Opt (Option):
        #   0 => One copy for all build types.
        #   1 => One for each build types.
        # Name:
        #   Name of directory to copy.
        self.mInstallOpts               = []


        # ====================
        # Misc
        # ====================
        self.mCMakeName                 = "" # Name used in IncludeSdk.cmake.
        self.mDepsInfoUsage             = [] # List of dependencies usage.
        self.mPkgconfig                 = "" # Pkgconfig settings.
        self.mReadMe                    = "" # Information in read me.
        self.mCompiler                  = Compiler()

        # Prefix name that the compiler creates during install
        # Ex (XCode):
        # /SDK/allvm-9.0-x86_64/Public/myLib/1.0-allvm-9.0-x86_64/lib/Debug
        self.mCompilerInstallPrefix     = False

        # Temporary variables.
        self.mBuildStatusDebug          = ""
        self.mBuildStatusRelease        = ""
        Library.mLibraries.append(self)


    # Copy into SDK.
    def _copyIntoSdk(self, build_type, install_dir, subpath=""):
        logMessage("-- START -------------------------------")
        logMessage("[Install Option]")
        # ====================
        # Library & Include
        # ====================
        createDirectory("library directory", self.mPathInfo.mLibraryRoot)
        installInclude  = os.path.join(install_dir, "include")
        installLibrary  = os.path.join(install_dir, "lib")
        if subpath:
            installLibrary = os.path.join(installLibrary, subpath)
        sdkLibrary = os.path.join(self.mPathInfo.mLibraryRoot, build_type)
        if not isExists(self.mPathInfo.mIncludeRoot):
            copyDirectory(installInclude, self.mPathInfo.mIncludeRoot)
        copyDirectory(installLibrary, sdkLibrary)


        # ====================
        # Custom
        # ====================
        for opts in self.mInstallOpts:
            opt  = opts[0]
            name = opts[1]
            installDir = os.path.join(install_dir, name)

            logMessage("Processing directory (" + name + ") BuildType: " + str(opt) + ")")
            if isExists(installDir):
                sdkDir = os.path.join(self.mPathInfo.mSdkRoot, name)

                # True for creating the subpath of build type.
                # Otherwise copy once.
                if opt:
                    logMessage("Set build type (" + build_type + ").")
                    sdkDir = os.path.join(sdkDir, build_type)

                logMessage("Looking for " + sdkDir + ".")
                if not isExists(sdkDir):
                    logMessage("Start the copy process.")
                    copyDirectory(installDir, sdkDir)
                else:
                    logMessage("Directory exists, skip the copy process.")
            else:
                logMessage("Directory (" + installDir + ") doesn't exists. Skip processing.")
        logMessage("-- END  -------------------------------")


    # Check result.
    def _checkBuildingResult(self, debug, release, resD, resR):
        if not resD and not resR:
            if debug:
                self.mBuildStatusDebug   = "FAILED"
            else:
                self.mBuildStatusDebug   = "DISABLED"

            if release:
                self.mBuildStatusRelease = "FAILED"
            else:
                self.mBuildStatusRelease = "DISABLED"
            return False
        elif resD and resR:
            self.mBuildStatusDebug   = "COMPLETED"
            self.mBuildStatusRelease = "COMPLETED"
        else:
            if debug:
                if resD:
                    self.mBuildStatusDebug = "COMPLETED"
                else:
                    self.mBuildStatusDebug = "FAILED"
            else:
                self.mBuildStatusDebug = "DISABLED"

            if release:
                if resR:
                    self.mBuildStatusRelease    = "COMPLETED"
                else:
                    self.mBuildStatusRelease    = "FAILED"
            else:
                self.mBuildStatusRelease    = "DISABLED"

        logBuilding(self.mName, "(DEBUG)   ** " + self.mBuildStatusDebug + " **")
        logBuilding(self.mName, "(RELEASE) ** " + self.mBuildStatusRelease + " **")
        return True


    # Do CMake.
    def _doCMake(
        self, buildType, checkFile, commonArgs = [],
        specificArgs = [], makeArgs = [], cmakelistDir = ""):
        logMessage("********************")
        logMessage("* CMake")
        logMessage("* BuildType: " + buildType)
        logMessage("********************")

        # We want only to generate, compile or install if there is unsuccessfull
        # process or not startet.
        logMessage("Checking file: " + checkFile)
        if not isExists(checkFile):
            logMessage(checkFile + " doesn't exists. Start compiling process....")

            # Make a copy, for not modifying the original.
            args = []
            copyArguments(commonArgs, args)
            copyArguments(specificArgs, args)
            installDir = os.path.join(self.mPathBuild, buildType, "install")

            # Change directory is perform here.
            cmake = CMake(self.mCompiler)
            if not cmake.generate(
                self.mPathBuild, buildType, cmakelistDir,
                installDir, args):
                return 0

            # Compiler selection.
            installPrefix = ""
            if self.mCompilerInstallPrefix:
                installPrefix = buildType.title()

            if self.mCompiler.isXcode():
                xcode = XCode()
                if not xcode.compile(buildType, makeArgs):
                    return 0

                if not xcode.install(buildType):
                    return 0

            else:
                make = Make(self.mCompiler)
                if not make.compile(makeArgs):
                    return 0

                if not make.install():
                    return 0

            self._copyIntoSdk(buildType, installDir, installPrefix)
        else:
            logMessage(checkFile + " already exists. ** SKIP ** processing CMake.")
            return 2
        return 1


    # Do configure.
    def _doConfigure(
        self, buildType, checkFile, commonArgs = [], specificArgs = [],
        makeArgs = [], useAutogen = False, useAutogenInSrc = False):
        logMessage("********************")
        logMessage("* Configure")
        logMessage("* BuildType: " + buildType)
        logMessage("********************")
        logMessage("Checking file: " + checkFile)
        if not isExists(checkFile):
            # Make a copy, for not modifying the original.
            args = []
            copyArguments(commonArgs, args)
            copyArguments(specificArgs, args)

            logConfigure(self.mName)
            conf = Configure()

            # Create directory if not exists.
            dir = os.path.join(self.mPathBuild, buildType)
            createDirectory("build directory", dir)
            installDir = os.path.join(dir, "install")

            if useAutogen:
                if not conf.autogen(dir, self.mPathSource, self.mPathInfo.mSdkRoot, useAutogenInSrc):
                    return 0

            if not conf.configure(dir, self.mPathSource, installDir, args):
                return 0

            make = Make()
            if not make.compile(makeArgs):
                return 0

            if not make.install():
                return 0

            self._copyIntoSdk(buildType, installDir)
            logConfigure(self.mName, "complete")
        else:
            logMessage(checkFile + " already exists. ** SKIP ** processing configure.")
            return 2
        return 1


    # Set internal states.
    def _setSystemUsage(self, value):
        self.mSystemUsage = (value == 1)


    # Add install option.
    def addInstallOpt(self, opt, name):
        self.mInstallOpts.append([opt, name])


    # Copy pkgconfig.
    def copyPkgconfig(self, name, path = ""):
        if path != "":
            src = os.path.join(path, name)
        else:
            src = os.path.join(self.mPathInfo.mLibraryRelease, "pkgconfig", name)

        if isExists(src):
            dst = os.path.join(sdkRoot.InfoPath.mPkgconfig, name)
            shutil.copy(src, dst)
            if isExists(dst):
                logMessage("Copy pkgconfig file (" + src + ") to (" + dst + ").")
                return True
            else:
                logMessage("Failed copy pkgconfig file (" + src + ") to (" + dst + ").")
                return False
        else:
            logMessage("Missing pkgconfig file (" + src + ")")
            return False


    # Exists.
    def existLibraryDebug(self):
        checkFile = self.mPathInfo.mFileDebug
        if self.mSystemUsage and self.mSystemInfo:
            checkFile = self.mPathSystem.mFileDebug

        return isExists(checkFile)

    def existLibraryRelease(self):
        checkFile = self.mPathInfo.mFileRelease
        if self.mSystemUsage and self.mSystemInfo:
            checkFile = self.mPathSystem.mFileRelease

        return isExists(checkFile)

    def existLibrary(self):
        debugExists   = self.existLibraryDebug()
        releaseExists = self.existLibraryRelease()
        return (debugExists or releaseExists)


    # Find package.
    def findPackage(self, name):
        for pkg in Library.mLibraries:
            if name == pkg.mName:
                return pkg

        logMessage(("Missing package (" + name + ") in", self.mName))
        return False


    # Footer.
    def footer(self):
        logMessage("========================================")
        logMessage(("= FINISHED", self.mName))
        logMessage("========================================")


    # Generate default library name.
    def generateDefaultName(self, name, debug_postfix = "_d", static = False):
        prefix  = "lib"
        postfix = ".so"
        if sdkRoot.Platform.isWindows():
            prefix = ""
            postfix = ".dll"
            if static:
                postfix = ".lib"
        else:
            if sdkRoot.Platform.isMacOsx():
                postfix = ".dylib"
                if static:
                    postfix = ".a"

        self.setLibraryNameDebug(name + debug_postfix, prefix, postfix)
        self.setLibraryNameRelease(name, prefix, postfix)
        self.mLibraryName = name

        # As default update system variables as specified.
        self.mPathSystem = copy.copy(self.mPathInfo)

    # Generate OSX framework name.
    def generateOsxFrameworkName(self, name, subpath_debug = "", subpath_release = ""):
        self.setLibraryNameDebug(name, "", ".framework", subpath_debug)
        self.setLibraryNameRelease(name, "", ".framework", subpath_release)
        self.mLibraryName = name


    # Get include path.
    def getInclude(self):
        if self.mSystemUsage and self.mSystemInfo:
            return self.mPathSystem.mInclude
        return self.mPathInfo.mInclude

    # Get debug library.
    def getLibraryDebug(self):
        if self.mSystemUsage and self.mSystemInfo:
            return self.mPathSystem.mFileDebug

        if self.existLibraryDebug():
            return self.mPathInfo.mFileDebug
        return self.mPathInfo.mFileRelease

    # Get release library.
    def getLibraryRelease(self):
        if self.mSystemUsage and self.mSystemInfo:
            return self.mPathSystem.mFileRelease
        return self.mPathInfo.mFileRelease

    # Get debug library path.
    def getPathDebug(self):
        if self.mSystemUsage and self.mSystemInfo:
            return self.mPathSystem.mLibraryDebug
        return self.mPathInfo.mLibraryDebug

    # Get release library path.
    def getPathRelease(self):
        if self.mSystemUsage and self.mSystemInfo:
            return self.mPathSystem.mLibraryRelease
        return self.mPathInfo.mLibraryRelease


    # Header.
    def header(self):
        logMessage("========================================")
        logMessage(("= PROCESSING", self.mName))
        logMessage("========================================")
        logMessage(("Prefix:       ", self.mPrefix))
        logMessage(("Version:      ", self.mVersion))
        logMessage(("URL:          ", self.mUrl))
        logMessage(("Source Dir:   ", self.mPathSource))
        logMessage(("Build Dir:    ", self.mPathBuild))
        logMessage(("SDK Dir:      ", self.mPathInfo.mSdkRoot))
        if self.mDepsInfoUsage:
            logMessage("--------------------")
            logMessage("- Dependencies")
            logMessage("--------------------")
            for v in self.mDepsInfoUsage:
                logMessage((" *", v))
            logMessage("--------------------")


    # Info usage.
    def infoUsage(self, pkg):
        msg = pkg.mName
        if (pkg.mSystemUsage or pkg.mPrecompiled):
            msg += " (System)"
        else:
            msg += " (SDK Library) -> " + pkg.mPathInfo.mSdkRoot
        self.mDepsInfoUsage.append(msg)

        if self.mReadMe:
            self.mReadMe.mDeps.append(pkg.mName)


    # Rename.
    def renameSourceDir(self, name):
        dir = os.path.join(self.mPathSourceRoot, name)
        if isExists(dir):
            os.rename(dir, self.mPathSource)
            logMessage("Rename dir [" + dir + "] to [" + self.mPathSource + "]")


    # Run CMake.
    def runCMake(
        self, debug, release, commonArgs = [], debugArgs = [],
        releaseArgs = [], makeArgs = [], cmakelistDir = ""):
        logBuilding(self.mName)
        resD = False
        resR = False

        # Directory to CMakeList.txt
        if cmakelistDir:
            cmakelistFile = os.path.join(self.mPathSource, cmakelistDir)
        else:
            cmakelistFile = self.mPathSource

        # Running debug if specified.
        if debug:
            resD = self._doCMake(
                sdkRoot.InfoPath.mPrefixDebug, self.mPathInfo.mFileDebug,
                commonArgs, debugArgs, makeArgs, cmakelistFile)

        # Running release if specified.
        if release:
            resR = self._doCMake(
                sdkRoot.InfoPath.mPrefixRelease, self.mPathInfo.mFileRelease,
                commonArgs, releaseArgs, makeArgs, cmakelistFile)

        # Show result.
        if not self._checkBuildingResult(debug, release, resD, resR):
            return False
        return True


    # Run configure.
    def runConfigure(self, debug, release, commonArgs = [], debugArgs = [],
        releaseArgs = [], makeArgs = [], useAutogen = False,
        useAutogenInSrc = False):
        logBuilding(self.mName)
        resD = False
        resR = False

        # Running debug if specified.
        if debug:
            # Build.
            resD = self._doConfigure(
                sdkRoot.InfoPath.mPrefixDebug,
                self.mPathInfo.mFileDebug,
                commonArgs,
                debugArgs,
                makeArgs,
                useAutogen,
                useAutogenInSrc
            )

            # Update system variables if not specified before.
            if resD and self.mSystemInfo == False:
                self.mPathSystem.mFileDebug = self.mPathInfo.mFileDebug


        # Running release if specified.
        if release:
            # Build.
            resR = self._doConfigure(
                sdkRoot.InfoPath.mPrefixRelease,
                self.mPathInfo.mFileRelease,
                commonArgs,
                releaseArgs,
                makeArgs,
                useAutogen,
                useAutogenInSrc
            )

            # Update system variables if not specified before.
            if resR and self.mSystemInfo == False:
                self.mPathSystem.mFileRelease = self.mPathInfo.mFileRelease


        # Show result.
        if not self._checkBuildingResult(debug, release, resD, resR):
            return False

        # Update system variables if not specified before.
        if self.mSystemInfo == False:
            self.mPathSystem.mInclude = self.mPathInfo.mInclude
        return True


    # Save CMake and Pkgconfig.
    def saveSdkFiles(self):
        # IncludeSdkFile is handle by package "sdkUtilIncludeSdkFile".

        if self.mPkgconfig:
            self.mPkgconfig.save()


    # Set CMake name.
    def setCMakeInfo(self, name):
        self.mCMakeName = name


    # Set compiler install prefix.
    def setCompilerInstallPrefix(self, value):
        self.mCompilerInstallPrefix = value


    # Set include prefix.
    def setInclude(self, name):
        if name:
            self.mPathInfo.mInclude = os.path.join(self.mPathInfo.mIncludeRoot, name)


    # Set library name.
    def setLibraryNameDebug(self, name, prefix, postfix, path = ""):
        self.mNameDebug         = name
        self.mLibraryNameDebug  = prefix + name + postfix
        if path:
            self.mPathInfo.mFileDebug = os.path.join(
                self.mPathInfo.mLibraryDebug, path, self.mLibraryNameDebug)
        else:
            self.mPathInfo.mFileDebug = os.path.join(
                self.mPathInfo.mLibraryDebug, self.mLibraryNameDebug)


    # Set library name.
    def setLibraryNameRelease(self, name, prefix, postfix, path = ""):
        self.mNameRelease           = name
        self.mLibraryNameRelease    = prefix + name + postfix
        if path:
            self.mPathInfo.mFileRelease = os.path.join(
                self.mPathInfo.mLibraryRelease, path, self.mLibraryNameRelease)
        else:
            self.mPathInfo.mFileRelease = os.path.join(
                self.mPathInfo.mLibraryRelease, self.mLibraryNameRelease)


    # Set system include prefix.
    def setSystemInfo(self, root, sub_include="", sub_lib="",
                      debug_name="", debug_file="",
                      release_name="", release_file=""):
        self.mPathSystem.mSdkRoot = root

        # Include.
        self.mPathSystem.mIncludeRoot   = os.path.join(root, "include")
        self.mPathSystem.mInclude       = self.mPathSystem.mIncludeRoot
        if sub_include:
            self.mPathSystem.mInclude = os.path.join(self.mPathSystem.mIncludeRoot, sub_include)

        # Library.
        self.mPathSystem.mLibraryRoot = os.path.join(root, "lib")
        if sub_lib:
            self.mPathSystem.mLibraryRoot = os.path.join(self.mPathSystem.mLibraryRoot, sub_lib)

        self.mPathSystem.mLibraryDebug      = os.path.dirname(debug_file)
        self.mPathSystem.mLibraryRelease    = os.path.dirname(release_file)
        self.mPathSystem.mFileDebug         = debug_file
        self.mPathSystem.mFileRelease = release_file

        # Update states.
        self.mSystemInfo = True


    # Set PKG info.
    def setPkgconfigInfo(self, pkg):
        self.mPkgconfig = pkg
        self.mPkgconfig.mDesc       = self.mDesc
        self.mPkgconfig.mWebsite    = self.mWebsite
        self.mPkgconfig.mVersion    = self.mVersion
        self.updatePkgconfigInfo()


    # Set README info.
    def setReadMeInfo(self, readme):
        self.mReadMe = readme
        self.mReadMe.mDesc      = self.mDesc
        self.mReadMe.mName      = self.mName
        self.mReadMe.mRevision  = self.mRevision
        self.mReadMe.mUrl       = self.mUrl
        self.mReadMe.mVersion   = self.mVersion
        self.mReadMe.mWebsite   = self.mWebsite


    # Update from archive.
    def updateFromArchive(self):
        srcFile = os.path.join(
            os.getcwd(), "Sources", self.mPrefix, self.mVersion + ".tbz2"
        )
        if isExists(srcFile):
            logging("Updating from archive", "", srcFile)
            file = tarfile.open(srcFile, "r:bz2")
            file.extractall(self.mPathSource)
            file.close()


    # Update library as the same as system.
    def updateLibraryAsPrecompiled(self):
        self.mPathInfo      = self.mPathSystem
        self.mSystemUsage   = True
        self.mPrecompiled   = True


    # Update PKG info.
    def updatePkgconfigInfo(self):
        self.mPkgconfig.mPrefix = self.mPathInfo.mSdkRoot
        if isExists(self.mPathInfo.mFileDebug):
            self.mPkgconfig.mLibraryNameDebug = self.mNameDebug
        if isExists(self.mPathInfo.mFileRelease):
            self.mPkgconfig.mLibraryNameRelease = self.mNameRelease
