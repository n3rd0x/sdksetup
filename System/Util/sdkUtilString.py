# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------


# ------------------------------------------------------------
# - String Converter
# ------------------------------------------------------------
def toString(msg) -> str:
    if not msg is None:
        if type(msg) is str:
            #print("[DEBUG] ---------- str")
            return msg
        elif type(msg) is tuple:
            #print("[DEBUG] ---------- tuple")
            return ' '.join(msg)
        elif type(msg) is list:
            #print("[DEBUG] ---------- list")
            return ' '.join(msg)
        elif type(msg) is bytes:
            #print("[DEBUG] ---------- byte")
            return msg.decode().rstrip()
        else:
            #print("[DEBUG] ---------- unknown")
            return msg
    #print("[DEBUG] ---------- None")
    return ""

