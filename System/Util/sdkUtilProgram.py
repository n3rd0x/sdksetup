# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
#import pathlib
import subprocess

# Local import.
from System import sdkRoot
from System.Util.sdkUtilLog import logMessage
from System.Util.sdkUtilLog import logLooking
from System.Util.sdkUtilString import toString
from System.Util.sdkUtilProcess import runProcess




# ------------------------------------------------------------
# - Program
# ------------------------------------------------------------
class Program:
    # Initialisation.
    def __init__(self, name, prog, website = ""):
        self.mName  = name
        self.mProg  = prog
        self.mWebsite = website
        self.mFound = False


    # Check existance.
    def exists(self):
        logLooking(self.mName)

        if sdkRoot.Platform.isWindows():
            process = subprocess.Popen(["where", self.mProg], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            process = subprocess.Popen(["which", self.mProg], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        if process.returncode >= 1:
            logLooking(self.mName, "missing")
            return False
        else:
            self.mFound = True
            msg = out.decode().rstrip()
            logLooking(self.mName, "found: " + msg)
        return True


    # Help.
    def help(self):
        if self.mWebsite:
            return self.Name + "(" + self.mWebsite + ")"
        else:
            return self.mName


    # Info.
    def info(self):
        return ""


    # Run.
    def run(self, path = "", args = [], logFile = ""):
        # Create program with arguments.
        prog = [self.mProg]
        for arg in args:
            prog.append(arg)

        # Change working directory if specified.
        if path:
            os.chdir(path)

        # Run the process.
        return runProcess(prgo, self.mName, logFile, "wb")

