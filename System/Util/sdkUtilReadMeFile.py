# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
#import subprocess
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLog import logMessage


# ------------------------------------------------------------
# - README Data
# ------------------------------------------------------------
class ReadMeData:
    def __init__(self):
        # User properties.
        self.mCMakeOpts = []
        self.mOptions   = []
        self.mDeps      = []

        # Library properties.
        self.mDesc      = ""
        self.mName      = ""
        self.mRevision  = ""
        self.mUrl       = ""
        self.mVersion   = ""
        self.mWebsite   = ""



# ------------------------------------------------------------
# - README
# ------------------------------------------------------------
class ReadMeFile:
    # Initialisation.
    def __init__(self, header = []):
        self.lines = []
        self.lines.append("# ************************************************************")
        self.lines.append("# SDK Package Information")
        self.lines.append("# ************************************************************")
        if header:
            copyArguments(header, self.lines)
        self.lines.append("")


    # Add package (ReadMeData)
    def add(self, data):
        if not data:
            return False
        if not data.mName:
            return False

        self.lines.append("# " + data.mName)
        if data.mVersion:
            self.lines.append("# Version:      " + data.mVersion)
        if data.mRevision:
            self.lines.append("# Revision:     " + data.mRevision)
        if data.mDesc:
            self.lines.append("# Description:  " + data.mDesc)
        if data.mWebsite:
            self.lines.append("# Website:      " + data.mWebsite)
        if data.mUrl:
            self.lines.append("# Download:     " + data.mUrl)

        opts = ""
        for opt in data.mOptions:
            opts += "'" + opt + "' "

        copts = ""
        for opt in data.mCMakeOpts:
            copts += opt + " "

        deps = ""
        for dep in data.mDeps:
            deps += "(" + dep + ") "

        if opts:
            self.lines.append("# Options:      " + opts)

        if copts:
            self.lines.append("# CMake Options:\r\n" + copts)

        if deps:
            self.lines.append("# Dependencies: " + deps)

        self.lines.append("\r\n")


    # Save.
    def save(self):
        readmeFile = os.path.join(sdkRoot.InfoPath.mSdkRoot, "README.txt")
        file = open(readmeFile, "w")
        for line in self.lines:
            file.write(line)
            file.write("\r\n")
        file.close()
        logMessage("Creating README file [" + readmeFile + "].")

