# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import subprocess
from System.Util.sdkUtilLog import logMessage
from System.Util.sdkUtilString import toString


# ------------------------------------------------------------
# - Run Process
# ------------------------------------------------------------
# Running a process and log to file.
def runProcess(args: list, prefix = "Process", logFile = "", mode = "wb"):
    argsstr = toString(args)
    logMessage("-- START -------------------------------")
    logMessage(("[" + prefix+ "]", argsstr))
    logMessage("----------------------------------------")
    if logFile:
        process = subprocess.Popen(
            argsstr, stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT, bufsize=1, shell=True
        )
        with process.stdout, open(logFile, mode) as file:
            for line in process.stdout:
                #sys.stdout.buffer.write(line)
                file.write(line)
                print(line.decode().rstrip())
    else:
        process = subprocess.Popen(argsstr, shell=True)
    process.wait()

    success = (process.returncode == 0)
    logMessage(("[" + prefix + "] Return Code:", str(process.returncode)))
    if success:
        logMessage(("[" + prefix + "] SUCCESS"))
    else:
        logMessage(("[" + prefix + "] FAILED"))
    logMessage("-- END  -------------------------------")
    return success

