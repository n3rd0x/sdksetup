# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
from System import sdkRoot
from System.Util.sdkUtilLog import *
from System.Util.sdkUtilCompiler import *
from System.Util.sdkUtilCore import createDirectory
from System.Util.sdkUtilProcess import runProcess
from System.Util.sdkUtilString import toString


# ------------------------------------------------------------
# - CMake
# ------------------------------------------------------------
class CMake:
    # Initialisation.
    def __init__(self, compiler = ""):
        self.mCompiler = compiler
        if not self.mCompiler:
            self.mCompiler = Compiler()
        if self.mCompiler.isUnknown():
            self.mCompiler.setDefault()
        self._setGenerator(self.mCompiler.getCompiler())


    # Set generator.
    def _setGenerator(self, compiler):
        if compiler == COMPILER_XCODE:
            self.mGenerator = "\"Xcode\""
        elif compiler == COMPILER_MSVC_NMAKE:
            self.mGenerator = "\"NMake Makefiles\""
            #self.mGenerator = "\"MSYS Makefiles\""
        else:
            self.mGenerator = "\"Unix Makefiles\""


    ##  Generate.
    # @param buildDir Building directory.
    # @param buildType Build type, usual values is either (Debug, Release).
    # @param cmakelistDir Directory to where CMakeList.txt is located.
    # @param installDir Installation directory.
    # @param args Arguments for CMake.
    def generate(self, buildDir, buildType, cmakelistDir, installDir, args = []):
        # Full directory to the build directory.
        dir = os.path.join(buildDir, buildType)
        createDirectory("build directory", dir)

        # Full program with arguments.
        logging("Generating project files", "[" + dir + "]")
        logMessage("Compiler specified: " + self.mCompiler.toString())
        cmake = []
        cmake.append("cmake")
        cmake.append("-G")
        cmake.append(self.mGenerator)
        cmake.append("-DCMAKE_BUILD_TYPE=" + buildType)
        cmake.append("-DCMAKE_INSTALL_PREFIX=" + installDir)
        cmake.append("--dir")
        cmake.append(dir)
        for arg in args:
            cmake.append(arg)
        cmake.append(cmakelistDir)

        # Process CMake.
        os.chdir(dir)
        if not runProcess(cmake, "CMakeGenerator", "sdkCMake.log"):
            logMessage("Failed to generate project files.")
            return False

        logging("Generating project files", "[" + dir + "]", "completed")
        return True
