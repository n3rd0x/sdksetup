# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
import subprocess
from System import sdkRoot
from System.Util.sdkUtilCore import generateTimestamp
from System.Util.sdkUtilCore import createDirectory
from System.Util.sdkUtilCore import isExists
from System.Util.sdkUtilLog import logMessage


# ------------------------------------------------------------
# - Pkgconfig File
# ------------------------------------------------------------
class PkgconfigFile:
    # Initialisation.
    def __init__(self, fileName, packageName):
        # User properties.
        self.mFileName          = fileName
        self.mPackageName       = packageName
        self.mDesc              = ""
        self.mRequires          = ""
        self.mPrivatesDebug     = ""
        self.mPrivatesRelease   = ""

        # Library properties.
        self.mLibraryNameDebug      = ""
        self.mLibraryNameRelease    = ""
        self.mPrefix                = ""
        self.mVersion               = ""
        self.mWebsite               = ""


    # Save file.
    def _doSave(self, file, privates, libName, libTarget):
        logMessage("Creating a custom pkgconfig file [" + file + "].")

        if isExists(file):
            logMessage("Removing Pkgconfig file [" + file + "] for update.")
            os.remove(file)

        pkg = open(file, "w")
        pkg.write("# ************************************************************\r\n")
        pkg.write("# Auto Generated Pkgconfig\r\n")
        pkg.write("# Generated " + generateTimestamp() + "\r\n")
        pkg.write("# ************************************************************\r\n")
        pkg.write("Prefix=" + self.mPrefix + "\r\n")
        pkg.write("LibDir=${Prefix}/lib/" + libTarget + "\r\n")
        pkg.write("IncludeDir=${Prefix}/include\r\n")
        pkg.write("\r\n")
        pkg.write("Name: " + self.mPackageName + "\r\n")
        pkg.write("Description: " + self.mDesc + "\r\n")
        pkg.write("Version: " + self.mVersion + "\r\n")
        pkg.write("Website: " + self.mWebsite + "\r\n")
        pkg.write("\r\n")
        if self.mRequires:
            pkg.write("Requires:\r\n")
        pkg.write("Cflags: -I${IncludeDir}\r\n")
        pkg.write("Libs: -L${LibDir} -l" + libName + "\r\n")
        if privates:
            pkg.write("Libs.private: " + privates + "\r\n")
        pkg.close()
        logMessage("Save SDK Pkgconfig file [" + file + "]")


    # Save into file.
    def save(self):
        if not self.mFileName:
            return False

        if not self.mLibraryNameDebug and self.mLibraryNameRelease:
            return False

        name = self.mFileName + ".pc"
        if self.mLibraryNameDebug:
            createDirectory("PKG debug", sdkRoot.InfoPath.mPkgconfigDebug)
            file = os.path.join(sdkRoot.InfoPath.mPkgconfigDebug, name)
            self._doSave(file, self.mPrivatesDebug, self.mLibraryNameDebug, "debug")

        if self.mLibraryNameRelease:
            createDirectory("PKG release", sdkRoot.InfoPath.mPkgconfigRelease)
            file = os.path.join(sdkRoot.InfoPath.mPkgconfigRelease, name)
            self._doSave(file, self.mPrivatesRelease, self.mLibraryNameRelease, "release")

