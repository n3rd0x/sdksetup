# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilLog import *
from System.Util.sdkUtilCore import createDirectory
from System.Util.sdkUtilProcess import runProcess


# ------------------------------------------------------------
# - Configure
# ------------------------------------------------------------
class Configure:
    # Autogen.
    def autogen(self, buildDir, sourceDir, installDir, forceAutogen = False):
        if not isExists(buildDir):
            logMessage("Path [" + buildDir + "] doesn't exists. ** STOP ** configure process.")
            return False
        os.chdir(buildDir)

        # We have to run the Autogen in the source directory.
        if forceAutogen:
            os.chdir(sourceDir)

        configureFile = os.path.join(sourceDir, "configure")
        if not isExists(configureFile) or forceAutogen:
            logging("Autogen", "[" + buildDir + "]")
            autogen = [
                os.path.join(sourceDir, "autogen.sh"),
                "--prefix=" + installDir
            ]

            logFile = os.path.join(buildDir, "sdkBuildAutogen.log")
            return runProcess(autogen, "Autogen", logFile)
        else:
            logMessage("Configure exists. ** SKIP ** autogen process.")

        return True


    # Configure.
    def configure(self, buildDir, sourceDir, installDir, args = []):
        if not isExists(buildDir):
            logMessage("Path [" + buildDir + "] doesn't exists. ** STOP ** configure process.")
            return False

        os.chdir(buildDir)
        logConfigure("project file", buildDir)
        conf = []
        conf.append(os.path.join(sourceDir, "configure"))
        conf.append("--prefix=" + installDir + "")
        for arg in args:
            conf.append(arg)

        if not runProcess(conf, "Configure", "sdkConfigure.log"):
            return False

        logConfigure("project file", "success")
        return True
