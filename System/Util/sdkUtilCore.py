# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import datetime
import errno
import os
import pathlib
import shutil

# Local import.
from System.Util.sdkUtilPrint import printChecking
from System.Util.sdkUtilPrint import printCopying




# ------------------------------------------------------------
# - Check Environment Path
# ------------------------------------------------------------
def checkPathEnv(env, value = ""):
    res = os.getenv(env, value)
    if not res == "":
        return os.path.abspath(res)
    return ""




# ------------------------------------------------------------
# - Copy Arguments
# ------------------------------------------------------------
def copyArguments(src, dst):
    # If we want a copy, we cannot use assign operator.
    # This would make a reference.
    for v in src:
        dst.append(v)




# ------------------------------------------------------------
# - Copy Directory
# ------------------------------------------------------------
def copyDirectory(src, dst):
    msg = "[" + src + "] to [" + dst + "]"
    printCopying(msg)
    if not isExists(src):
        printCopying(msg, "source not exists")
        return False

    try:
        shutil.copytree(src, dst)
        printCopying(msg, "completed")
        return True
    except OSError as e:
        printCopying(msg, "failed")
        print(e)
        return False




# ------------------------------------------------------------
# - Create Directory
# ------------------------------------------------------------
def createDirectory(name, dir):
    path = pathlib.Path(dir)
    printChecking(name)

    if not path.exists():
        printChecking(name, "not exists, creating at [" + dir + "]")
        path.mkdir(parents=True)
    else:
        printChecking(name, "found at " + dir)




# ------------------------------------------------------------
# - Generate Timestamp
# ------------------------------------------------------------
def generateTimestamp(huhamReadable = True):
    if huhamReadable:
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    else:
        return datetime.datetime.now().strftime("%Y%m%d%H%M%S")




# ------------------------------------------------------------
# - Check Existance of File or Directory
# ------------------------------------------------------------
def isExists(name):
    #print("NAME:", name)
    if name:
        path = pathlib.Path(name)
        #print("PATH:", path)
        if path.exists():
            #print("TRUE")
            return True
        else:
            #print("FALSE")
            return False
    else:
        #print("FALSE")
        return False

