# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
import tarfile

# Local import.
from System import sdkRoot
from System.Util.sdkUtilLog import *


# ------------------------------------------------------------
# - Tar
# ------------------------------------------------------------
class Tar:
    # Initialisation.
    def __init__(self, fileName, extension):
        self.mFileName  = fileName
        self.mExtension = extension


    # Extract.
    def extract(self, path, version):
        if not isExists(self.mFileName):
            logMessage("Stop extracting due to missing file (" + self.mFileName + ").")
            return False

        logExtracting(self.mFileName)
        dir = os.path.join(path, version)
        if not isExists(dir):
            logMessage("Extracting into [" + path + "].")
            tar = tarfile.open(self.mFileName, "r:" + self.mExtension)
            tar.extractall(path)
            tar.close()
        else:
            logMessage("Directory [" + dir + "] already exists. ** SKIP ** extracting process.")

        logExtracting(self.mFileName, "completed")
        return True




# ------------------------------------------------------------
# - TarBz2
# ------------------------------------------------------------
class TarBz2(Tar):
    # Initialisation.
    def __init__(self, fileName):
        Tar.__init__(self, fileName, "bz2")




# ------------------------------------------------------------
# - TarGz
# ------------------------------------------------------------
class TarGz(Tar):
    # Initialisation.
    def __init__(self, fileName):
        Tar.__init__(self, fileName, "gz")