# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import datetime
import os
import subprocess
from System import sdkRoot
from System.Util.sdkUtilLog import logMessage


# ------------------------------------------------------------
# - Export LD_LIBRARY
# ------------------------------------------------------------
class ExportSdkFile:
    # Initialisation.
    def __init__(self):
        self.paths = []
        self.lines = []
        self.lines.append("# ************************************************************")
        self.lines.append("# Export LD_LIBRARY")
        self.lines.append("# Generated " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        self.lines.append("# ************************************************************")
        self.lines.append("")
        self.export = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH"


    # Add package.
    def add(self, pkg):
        # Add only non-duplicate values.
        var     = ":" + pkg.getPathRelease()
        exists  = False
        for v in self.paths:
            if v == var:
                exists = True
                break

        if not exists:
            self.export += var
            self.paths.append(var)

    # Save.
    def save(self):
        sFile = os.path.join(sdkRoot.InfoPath.mSdkRoot, "ExportSdkFile.txt")
        file = open(sFile, "w")
        for line in self.lines:
            file.write(line)
            file.write("\n")
        file.write(self.export)
        file.close()
        logMessage("Creating SDK export file [" + sFile + "].")

