# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilString import toString


# ------------------------------------------------------------
# - Development Environment
# ------------------------------------------------------------
def displayDevEnvironment(listProgs = [], listLibs = []):
    msg = []
    msg.append("----------------------------------------")
    msg.append("- DEVELOPMENT ENVIRONMENT")
    msg.append("----------------------------------------")
    progs = displayDevEnvPrograms(listProgs)
    if progs:
        if sdkRoot.Platform.isUnixAlike():
            msg.append(toString(progs))
        else:
            msg.append(progs)

    libs = displayDevEnvLibraries(listLibs)
    if libs:
        if sdkRoot.Platform.isUnixAlike():
            msg.append(toString(libs))
        else:
            msg.append(libs)
    msg.append("----------------------------------------")
    return msg



# ------------------------------------------------------------
# - Development Environment Libraries
# ------------------------------------------------------------
def displayDevEnvLibraries(libs):
    msg = []
    return msg




# ------------------------------------------------------------
# - Development Environment Programs
# ------------------------------------------------------------
def displayDevEnvPrograms(progs):
    msg = []
    if sdkRoot.Platform.isUbuntu():
        msg.append("sudo apt-get install")
        msg.append("build-essential")
    elif sdkRoot.Platform.isMacOsx():
        msg.append("brew install")
    elif sdkRoot.Platform.isWindows():
        msg.append("Install Visual Studio")
        msg.append("Install DirectX (https://www.microsoft.com/en-us/download/details.aspx?id=6812)")
        msg.append("Install OpenAL (https://www.openal.org)")

    for p in progs:
        msg.append(p.info())
    return msg

