# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System.Util.sdkUtilString import toString


# ------------------------------------------------------------
# - Package Information
# ------------------------------------------------------------
class InfoPackage:
    # Initialise with default values.
    def __init__(self):
        # Installation directory in the real filesystem.
        self.mInstallDir       = ""

        # Maintainer info.
        self.mMaintainerName   = ""
        self.mMaintainerEmail  = ""

        # Architecture.
        self.mArch             = ""

        # Prefix for the package name.
        self.mPrefix    = ""


    # Print
    def print(self):
        for i in self.toString():
            print(toString(i))


    # To string.
    def toString(self):
        msg = []
        msg.append("----------------------------------------")
        msg.append("- PACKAGE INFO")
        msg.append("----------------------------------------")
        msg.append(("Maintainer (Name): ", self.mMaintainerName))
        msg.append(("Maintainer (Email):", self.mMaintainerEmail))
        msg.append(("Install Dir:       ", self.mInstallDir))
        msg.append(("Architecture:      ", self.mArch))
        msg.append(("Prefix:            ", self.mPrefix))
        msg.append("----------------------------------------")
        return msg




# ------------------------------------------------------------
# - Path Information
# ------------------------------------------------------------
class InfoPath:
    # Initialise with default values.
    def __init__(self):
        # Archive of source files.
        self.mArchive   = ""

        # Building directory.
        self.mBuild     = ""

        # CMake files (sub of SDK)
        self.mCMake     = ""

        # Package files (.deb etc) (sub of SDK)
        self.mPackage   = ""

        # Pkgconfig files (sub of SDK)
        self.mPkgconfig         = ""
        self.mPkgconfigDebug    = ""
        self.mPkgconfigRelease  = ""

        # Root SDK directory with suffix.
        self.mSdkRoot   = ""

        # Directory where libraries are installed (sub of SDK)
        self.mSdkLib    = ""

        # Source file of libraries.
        self.mSource    = ""

        # Suffix for the SDK root.
        self.mSuffix    = ""

        # Prefix for debug / release
        self.mPrefixDebug   = "debug"
        self.mPrefixRelease = "release"


    # Print
    def print(self):
        for i in self.toString():
            print(toString(i))


    # To string.
    def toString(self):
        msg = []
        msg.append("----------------------------------------")
        msg.append("- DIRECTORY INFO")
        msg.append("----------------------------------------")
        msg.append(("Archive:       ", self.mArchive))
        msg.append(("Source:        ", self.mSource))
        msg.append(("SDK (Root)):   ", self.mSdkRoot))
        msg.append(("SDK (Library)):", self.mSdkLib))
        msg.append(("Build:         ", self.mBuild))
        msg.append(("CMake:         ", self.mCMake))
        msg.append(("Package:       ", self.mPackage))
        msg.append(("Pkgconfig:     ", self.mPkgconfig))
        msg.append(("Suffix:        ", self.mSuffix))
        msg.append("----------------------------------------")
        return msg

