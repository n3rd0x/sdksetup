# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilProgram import Program


# ------------------------------------------------------------
# - Program Make
# ------------------------------------------------------------
class ProgramMake(Program):
    # Initialisation.
    def __init__(self):
        if sdkRoot.Platform.isWindows():
            Program.__init__(self, "NMake", "nmake", "https://www.visualstudio.com")
            #Program.__init__(self, "Make", "make")
        else:
            Program.__init__(self, "Make", "make")


    # Help.
    def help(self):
        if sdkRoot.Platform.isUbuntu():
            return "apt-get install build-essential"
        elif sdkRoot.Platform.isWindows():
            return "Install Visual Studio"
        return Program.help(self)


    # Info.
    def info(self):
        if sdkRoot.Platform.isUnixAlike():
            return "make"
        return Program.help(self)