# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramTar import TarGz
from System.Util.sdkUtilProgramWget import Wget
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library Zlib
# ------------------------------------------------------------
class LibraryZlib(Library):
    # Initialisation.
    def __init__(self, system_usage = False):
        self.mFileName = "zlib-1.2.11.tar.gz"
        Library.__init__(
            self,
            "Zlib",
            "Zlib",
            "1.2.11",
            "http://www.zlib.net/" + self.mFileName,
            "",
            "http://www.zlib.net",
            "zlib compression library"
        )

        # Set states.
        self._setSystemUsage(system_usage)

        # Generate as shared.
        self.generateDefaultName("z", "", False)

        # IncludeSdk.cmake
        self.setCMakeInfo("ZLIB_HOME")

        # System info.
        if sdkRoot.Platform.isMacOsx:
            self.setSystemInfo(
                "/opt/local",
                debug_file="/opt/local/lib/libz.dylib",
                release_file="/opt/local/lib/libz.dylib"
            )

        # Pkgconfig info.
        pkg = PkgconfigFile("zlib", "zlib")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()


        # ====================
        # Build Arguments
        # ====================
        #self.mArgs = [
        #    "--static"
        #]
        self.mArgs      = []
        self.mArgsD     = []
        self.mArgsR     = []
        self.mMakeArgs  = []

        if sdkRoot.Platform.isMachine_x86_64():
            self.mArgs.append("--64")
            self.mArgs.append("--archs=\"-arch x86_64\"")

        copyArguments(self.mArgs, readme.mOptions)
        self.setReadMeInfo(readme)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        res = self.runConfigure(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs
        )
        self.updatePkgconfigInfo()
        return res


    # Download.
    def download(self):
        res, file = self.downloadFile()
        if not res:
            return False

        tar = TarGz(file)
        tar.extract(self.mPathSourceRoot, self.mVersion)

        self.renameSourceDir("zlib-" + self.mVersion)
        return True


    # Download file.
    def downloadFile(self):
        wget = Wget(self.mFileName, self.mUrl)
        return wget.download(sdkRoot.InfoPath.mArchive)
