# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramGit import Git
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library Libpng
# ------------------------------------------------------------
class LibraryLibpng(Library):
    # Initialisation.
    def __init__(self):
        Library.__init__(
            self,
            "PNG",
            "Png",
            "1.6.37",
            "git://git.code.sf.net/p/libpng/code",
            "a40189cf881e9f0db80511c382292a5604c3c3d1",
            "http://www.libpng.org/pub/png/libpng.html",
            "Loads and saves PNG files"
        )
        self.generateDefaultName("png", "", False)

        # CMake info.
        self.setCMakeInfo("PNG_HOME")

        # System info.
        if sdkRoot.Platform.isMacOsx:
            self.setSystemInfo(
                "/opt/local",
                debug_file="/opt/local/lib/libpng.dylib",
                release_file="/opt/local/lib/libpng.dylib"
            )
            self.updateLibraryAsPrecompiled()

        # Pkgconfig.
        pkg = PkgconfigFile("libpng", "png")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()


        # ====================
        # Build Arguments
        # ====================
        self.mArgs      = []
        self.mArgsD     = []
        self.mArgsR     = []
        self.mMakeArgs  = []

        self.mArgs.append("-DPNG_TESTS:BOOL=OFF")
        self.mArgs.append("-DPNG_SHARED:BOOL=ON")
        self.mArgs.append("-DPNG_STATIC:BOOL=ON")

        if sdkRoot.Platform.isUnixAlike():
            self.mArgs.append("-DCMAKE_C_FLAGS=-fPIC")

        copyArguments(self.mArgs, readme.mCMakeOpts)
        self.setReadMeInfo(readme)


        # ====================
        # Dependencies
        # ====================
        zlipName = "Zlib"
        if sdkRoot.Platform.isWindows():
            zlipName = "OgreDepsZlib"
        zlib = self.findPackage(zlipName)
        if zlib:
            self.infoUsage(zlib)
            self.mArgs.append("-DZLIB_INCLUDE_DIR:PATH=" + zlib.getInclude())
            self.mArgs.append("-DZLIB_LIBRARY_DEBUG:FILEPATH=" + zlib.getLibraryDebug())
            self.mArgs.append("-DZLIB_LIBRARY_RELEASE:FILEPATH=" + zlib.getLibraryRelease())




    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        self.updateFromArchive()
        res =  self.runCMake(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs
        )
        self.updatePkgconfigInfo()
        return res


    # Download.
    def download(self):
        git = Git(self.mName, self.mPrefix, self.mVersion, self.mUrl, self.mRevision)
        return git.cloning(self.mPathSource)
