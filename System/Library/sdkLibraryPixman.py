# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilCore import createDirectory
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramGit import Git
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library Pixman
# ------------------------------------------------------------
class LibraryPixman(Library):
    # Initialisation.
    # Official repo: git://anongit.freedesktop.org/git/pixman.git
    # Community repo: https://github.com/CMakePorts/cmake.pixman.git
    def __init__(self):
        Library.__init__(
            self,
            "Pixman",
            "Pixman",
            "0.38.4",
            "https://gitlab.freedesktop.org/pixman/pixman.git",
            "e8df10eea9609609568bf4cbc05796594c1b978d",
            "https://cairographics.org",
            "The pixman library (version 1)"
        )
        # Set states.
        self.mSystemUsage = True

        # Generate as shared.
        self.generateDefaultName("pixman-1", "", False)
        self.setInclude("pixman-1")

        # CMake info.
        self.setCMakeInfo("PIXMAN_HOME")

        # System info.
        if sdkRoot.Platform.isMacOsx:
            self.setSystemInfo(
                "/opt/local",
                sub_include="pixman-1",
                debug_file="/opt/local/lib/libpixman-1.dylib",
                release_file="/opt/local/lib/libpixman-1.dylib"
            )
            self.updateLibraryAsPrecompiled()

        # Pkgconfig.
        pkg = PkgconfigFile("pixman-1", "pixman-1")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()


        # ====================
        # Build Arguments
        # ====================
        self.mArgs = [
            "--enable-gtk=no",
            "--enable-libpng=yes",
            "--enable-shared=yes",
            "--enable-static=no",
            "CFLAGS=-fPIC",
            "CXXFLAGS=-fPIC",
            "LIBS=\"-lz -lm\"",
        ]
        self.mArgsD     = [
            "PKG_CONFIG_PATH=" + sdkRoot.InfoPath.mPkgconfigDebug
        ]
        self.mArgsR     = [
            "PKG_CONFIG_PATH=" + sdkRoot.InfoPath.mPkgconfigRelease
        ]
        self.mMakeArgs  = []

        copyArguments(self.mArgs, readme.mOptions)
        self.setReadMeInfo(readme)


    # Create testing files.
    def _createTestFiles(self, buildType):
        nameRegionTest  = "region-test"
        nameScalingTest = "scaling-helpers-test"
        dir = os.path.join(self.mPathBuild, buildType, "pixman", "test")
        createDirectory("test", dir)
        fileRegionTest = os.path.join(dir, nameRegionTest)
        fileScalingTest = os.path.join(dir, nameScalingTest)
        file = open(fileRegionTest, "w")
        file.write("Check")
        file.close()
        file = open(fileScalingTest, "w")
        file.write("Check")
        file.close()


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        # Using system library.
        return True

        # Fails on compiling the test-programs.
        if sdkRoot.Platform.isMacOsx():
            if debug:
                self._createTestFiles(sdkRoot.InfoPath.mPrefixDebug)
            if release:
                self._createTestFiles(sdkRoot.InfoPath.mPrefixRelease)

        res = self.runConfigure(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs,
            True
        )
        self.updatePkgconfigInfo()
        return res


    # Download.
    def download(self):
        git = Git(self.mName, self.mPrefix, self.mVersion, self.mUrl, self.mRevision)
        return git.cloning(self.mPathSource)
