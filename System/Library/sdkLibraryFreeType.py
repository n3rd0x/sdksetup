# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2020 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library FreeType
# ------------------------------------------------------------
# TODO: Complete the manually process.
class LibraryFreeType(Library):
    # Initialisation.
    def __init__(self):
        Library.__init__(
            self,
            "FreeType",
            "FreeType",
            "",
            "",
            "",
            "",
            ""
        )
        # Generate as shared.
        self.generateDefaultName("freetype", "", False)

        # IncludeSdk.cmake
        self.setCMakeInfo("FREETYPE_HOME")

        # System info.
        if sdkRoot.Platform.isMacOsx:
            self.setSystemInfo(
                "/opt/local",
                sub_include="freetype2",
                debug_file="/opt/local/lib/libfreetype.dylib",
                release_file="/opt/local/lib/libfreetype.dylib"
            )
            self.updateLibraryAsPrecompiled()

        # Pkgconfig info.
        pkg = PkgconfigFile("FreeType", "FreeType")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()


        # ====================
        # Build Arguments
        # ====================
        self.mArgs      = []
        self.mArgsD     = []
        self.mArgsR     = []
        self.mMakeArgs  = []

        copyArguments(self.mArgs, readme.mOptions)
        self.setReadMeInfo(readme)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        return True


    # Download.
    def download(self):
        return True

