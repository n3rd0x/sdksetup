# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramGit import Git
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library Jsoncpp
# ------------------------------------------------------------
class LibraryJsoncpp(Library):
    # Initialisation.
    def __init__(self):
        Library.__init__(
            self,
            "Jsoncpp",
            "Jsoncpp",
            "1.8.4",
            "https://github.com/open-source-parsers/jsoncpp.git",
            "ddabf50f72cf369bf652a95c4d9fe31a1865a781",
            "https://github.com/open-source-parsers/jsoncpp",
            "A C++ library for interacting with JSON"
        )
        self.generateDefaultName("jsoncpp")
        self.setInclude("json")

        # IncludeSdk.cmake
        self.setCMakeInfo("JSONCPP_HOME")

        # Pkgconfig info.
        pkg = PkgconfigFile("jsoncpp", "jsoncpp")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()
        readme.mOptions = []




        # ====================
        # Build Arguments
        # ====================
        self.mArgs      = []
        self.mArgsD     = []
        self.mArgsR     = []
        self.mMakeArgs  = []

        self.mArgs.append("-DCMAKE_DEBUG_POSTFIX=_d")
        self.mArgs.append("-DBUILD_SHARED_LIBS:BOOL=ON")
        self.mArgs.append("-DBUILD_STATIC_LIBS:BOOL=ON")
        self.mArgs.append("-DJSONCPP_WITH_POST_BUILD_UNITTEST:BOOL=OFF")
        self.mArgs.append("-DJSONCPP_WITH_STRICT_ISO:BOOL=OFF")
        self.mArgs.append("-DJSONCPP_WITH_TESTS:BOOL=OFF")

        if sdkRoot.Platform.isUnixAlike():
            self.mArgs.append("-DCMAKE_C_FLAGS=-fPIC")
            self.mArgs.append("-DCMAKE_CXX_FLAGS=-fPIC")

        copyArguments(self.mArgs, readme.mCMakeOpts)
        self.setReadMeInfo(readme)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        self.updateFromArchive()
        res = self.runCMake(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs
        )
        self.updatePkgconfigInfo()
        return res



    # Download.
    def download(self):
        git = Git(self.mName, self.mPrefix, self.mVersion, self.mUrl, self.mRevision)
        return git.cloning(self.mPathSource)
