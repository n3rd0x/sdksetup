# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import os
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramMercurial import Mercurial
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library OgreDeps
# ------------------------------------------------------------
class LibraryOgreDeps(Library):
    # Initialisation.
    def __init__(self):
        Library.__init__(
            self,
            "OgreDeps",
            "OgreDeps",
            "20190919-ee570c5b",
            "https://bitbucket.org/cabalistic/ogredeps",
            "ee570c5b841b0944db565f4f4584a181502e986b"
        )
        self.generateDefaultName("OIS", "_d", True)
        self.setCompilerInstallPrefix(True)

        # ReadMe info.
        readme = ReadMeData()


        # ====================
        # Build Arguments
        # ====================
        self.mArgs = [
            "-DCMAKE_DEBUG_POSTFIX=_d"
        ]

        if sdkRoot.Platform.isWindows():
            # On unix-alike system, this wasn't set to be default.
            # So we are using our compiled version of SDL.
            # For consistency, we disable this build.
            self.mArgs.append("-DOGREDEPS_BUILD_SDL2:BOOL=OFF")
        if sdkRoot.Platform.isUnix():
            self.mArgs.append("-DCMAKE_C_FLAGS=-fPIC")
            self.mArgs.append("-DCMAKE_CXX_FLAGS=-fPIC")
        if sdkRoot.Platform.isMacOsx():
            self.mArgs.append("-DCMAKE_OSX_ARCHITECTURES=x86_64")

        self.mArgsD     = ["-DCMAKE_BUILD_TYPE=Debug"]
        self.mArgsR     = ["-DCMAKE_BUILD_TYPE=Release"]
        self.mMakeArgs  = []

        copyArguments(self.mArgs, readme.mOptions)
        self.setReadMeInfo(readme)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        res = self.runCMake(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs
        )
        return res


    # Download.
    def download(self):
        hg = Mercurial(
            self.mName, self.mPrefix, self.mVersion, self.mUrl, self.mRevision
        )
        return hg.cloning(self.mPathSource)




# ------------------------------------------------------------
# - Package OgreDeps Freeimage
# ------------------------------------------------------------
class LibraryOgreDepsFreeimage(Library):
    # Initialisation.
    def __init__(self):
        odeps = self.findPackage("OgreDeps")
        Library.__init__(
            self,
            "OgreDepsFreeimage",
            odeps.mPrefix,
            odeps.mVersion,
            odeps.mWebsite
        )
        self.generateDefaultName("freeimage", "_d", True)

        # CMake info.
        self.setCMakeInfo("FREEIMAGE_HOME")

        # Pkgconfig.
        pkg = PkgconfigFile("freeimage", "freeimage")
        self.setPkgconfigInfo(pkg)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        return True


    # Download.
    def download(self):
        return True



# ------------------------------------------------------------
# - Package OgreDeps Freetype
# ------------------------------------------------------------
class LibraryOgreDepsFreetype(Library):
    # Initialisation.
    def __init__(self):
        odeps = self.findPackage("OgreDeps")
        Library.__init__(
            self,
            "OgreDepsFreetype",
            odeps.mPrefix,
            odeps.mVersion,
            odeps.mWebsite
        )
        self.generateDefaultName("freetype", "_d", True)

        # CMake info.
        self.setCMakeInfo("FREETYPE_HOME")

        # Pkgconfig.
        pkg = PkgconfigFile("freetype2", "freetype")
        self.setPkgconfigInfo(pkg)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        return True


    # Download.
    def download(self):
        return True




# ------------------------------------------------------------
# - Package OgreDeps Zlib
# ------------------------------------------------------------
class LibraryOgreDepsZlib(Library):
    # Initialisation.
    def __init__(self):
        odeps = self.findPackage("OgreDeps")
        Library.__init__(
            self,
            "OgreDepsZlib",
            odeps.mPrefix,
            odeps.mVersion,
            odeps.mWebsite
        )
        self.generateDefaultName("zlib", "_d", True)

        # CMake info.
        self.setCMakeInfo("ZLIB_HOME")

        # Pkgconfig.
        pkg = PkgconfigFile("zlib", "zlib")
        self.setPkgconfigInfo(pkg)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        return True


    # Download.
    def download(self):
        return True




# ------------------------------------------------------------
# - Package OgreDeps ZZip
# ------------------------------------------------------------
class LibraryOgreDepsZZip(Library):
    # Initialisation.
    def __init__(self):
        odeps = self.findPackage("OgreDeps")
        Library.__init__(
            self,
            "OgreDepsZZip",
            odeps.mPrefix,
            odeps.mVersion,
            odeps.mWebsite
        )
        self.generateDefaultName("zziplib", "_d", True)

        # CMake info.
        self.setCMakeInfo("ZZIP_HOME")

        # Pkgconfig.
        pkg = PkgconfigFile("zzip", "zzip")
        self.setPkgconfigInfo(pkg)


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        return True


    # Download.
    def download(self):
        return True
