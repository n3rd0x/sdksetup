# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2020 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
from System import sdkRoot
from System.Util.sdkUtilCore import copyArguments
from System.Util.sdkUtilLibrary import Library
from System.Util.sdkUtilPkgconfigFile import PkgconfigFile
from System.Util.sdkUtilProgramGit import Git
from System.Util.sdkUtilReadMeFile import ReadMeData


# ------------------------------------------------------------
# - Library OGRE
# ------------------------------------------------------------
class LibraryOgre(Library):
    # Initialisation.
    def __init__(self, ogre_next = False):
        url         = "https://github.com/OGRECave/ogre.git"
        revision    = "bcb7b10af67fba9d1f22a4cac1aed971a80bd5c4"
        version     = "1.12.4"

        if ogre_next:
            url         = "https://github.com/OGRECave/ogre-next.git"
            revision    = "5b682fb90c9e8e660e2fbf92bbf7797a9246700d"
            version     = "2.1"

        Library.__init__(
            self,
            "OGRE",
            "Ogre",
            version,
            url,
            revision,
            "http://www.ogre3d.org",
            "Object-Oriented Graphics Rendering Engine"
        )

        # Generate as shared.
        self.generateDefaultName("OgreMain")
        #if sdkRoot.Platform.isMacOsx():
        #    self.generateOsxFrameworkName("OGRE", subpath_debug="Debug", subpath_release="Release")

        # Set include path.
        self.setInclude("OGRE")

        # IncludeSdk.cmake
        self.setCMakeInfo("OGRE_HOME")

        # Pkgconfig info.
        #if not sdkRoot.Platform.isMacOsx():
        pkg = PkgconfigFile("OGRE", "OGRE")
        self.setPkgconfigInfo(pkg)

        # ReadMe info.
        readme = ReadMeData()
        readme.mOptions = [
            "Double Precision",
            "Node Inherit Transformation"
        ]
        if not ogre_next:
            readme.mOptions.append("STL Thread")
            readme.mOptions.append("!Nodeless Positioning")
        readme.mDeps = []


        # ====================
        # Build Arguments
        # ====================
        self.mArgs      = []
        self.mArgsD     = []
        self.mArgsR     = []
        self.mMakeArgs  = []

        if ogre_next:
            self.mArgs.append("-DOGRE_BUILD_SAMPLES2:BOOL=OFF")
            self.mArgs.append("-DOGRE_CONFIG_THREAD_PROVIDER=0")
            self.mArgs.append("-DOGRE_CONFIG_THREADS=0")
        else:
            self.mArgs.append("-DOGRE_BUILD_DEPENDENCIES:BOOL=ON")
            self.mArgs.append("-DOGRE_NODELESS_POSITIONING:BOOL=OFF")
            self.mArgs.append("-DOGRE_CONFIG_THREAD_PROVIDER=std")
            self.mArgs.append("-DOGRE_CONFIG_THREADS=3")
            self.mArgsD.append("-Dpugixml_DIR:PATH=" + self.mPathBuild + "/debug/Dependencies/lib/cmake/pugixml")
            self.mArgsD.append("-Dpugixml_DIR:PATH=" + self.mPathBuild + "/release/Dependencies/lib/cmake/pugixml")

        self.mArgs.append("-DCMAKE_DEBUG_POSTFIX=_d")
        self.mArgs.append("-DOGRE_CONFIG_NODE_INHERIT_TRANSFORM:BOOL=ON")
        self.mArgs.append("-DOGRE_BUILD_TESTS:BOOL=OFF")
        self.mArgs.append("-DOGRE_BUILD_TOOLS:BOOL=ON")
        self.mArgs.append("-DOGRE_CONFIG_DOUBLE:BOOL=ON")
        self.mArgs.append("-DOGRE_COPY_DEPENDENCIES:BOOL=OFF")
        self.mArgs.append("-DOGRE_SIMD_NEON:BOOL=OFF")
        self.mArgs.append("-DOGRE_SIMD_SSE2:BOOL=OFF")
        self.mArgs.append("-DOGRE_INSTALL_DEPENDENCIES:BOOL=OFF")
        self.mArgs.append("-DOGRE_INSTALL_DOCS:BOOL=OFF")
        self.mArgs.append("-DOGRE_INSTALL_SAMPLES:BOOL=OFF")
        self.mArgs.append("-DOGRE_INSTALL_TOOLS:BOOL=ON")
        self.mArgs.append("-DOGRE_USE_BOOST:BOOL=OFF")

        if sdkRoot.Platform.isMacOsx():
            self.mArgs.append("-DOGRE_BUILD_LIBS_AS_FRAMEWORKS:BOOL=OFF")

        if sdkRoot.Platform.isUnixAlike():
            self.mArgs.append("-DCMAKE_CXX_FLAGS=-fPIC")

        # Install options.
        self.addInstallOpt(True, "bin")
        self.addInstallOpt(True, "CMake")

        if not ogre_next:
            self.addInstallOpt(False, "Media")

        copyArguments(self.mArgs, readme.mCMakeOpts)
        self.setReadMeInfo(readme)


        # ====================
        # Dependencies
        # ====================
        freeimage = self.findPackage("FreeImage")
        if freeimage:
            self.infoUsage(freeimage)
            self.mArgs.append("-DFreeImage_INCLUDE_DIR:PATH=" + freeimage.getInclude())
            self.mArgs.append("-DFreeImage_LIBRARY_DBG:FILEPATH=" + freeimage.getLibraryDebug())
            self.mArgs.append("-DFreeImage_LIBRARY_REL:FILEPATH=" + freeimage.getLibraryRelease())

        freetype = self.findPackage("FreeType")
        if freetype:
            self.infoUsage(freetype)
            self.mArgs.append("-DFREETYPE_INCLUDE_DIR_freetype2:PATH=" + freetype.getInclude())
            self.mArgs.append("-DFREETYPE_INCLUDE_DIR_ft2build:PATH=" + freetype.getInclude())
            self.mArgs.append("-DFREETYPE_LIBRARY_DEBUG:FILEPATH=" + freetype.getLibraryDebug())
            self.mArgs.append("-DFREETYPE_LIBRARY_RELEASE:FILEPATH=" + freetype.getLibraryRelease())

        zlib = self.findPackage("Zlib")
        if zlib:
            self.infoUsage(zlib)
            self.mArgs.append("-DZLIB_INCLUDE_DIR:PATH=" + zlib.getInclude())

            if ogre_next:
                self.mArgs.append("-DZLIB_LIBRARY_DBG:FILEPATH=" + zlib.getLibraryDebug())
                self.mArgs.append("-DZLIB_LIBRARY_REL:FILEPATH=" + zlib.getLibraryRelease())
            else:
                self.mArgs.append("-DZLIB_LIBRARY_DEBUG:FILEPATH=" + zlib.getLibraryDebug())
                self.mArgs.append("-DZLIB_LIBRARY_RELEASE:FILEPATH=" + zlib.getLibraryRelease())

        zzip = self.findPackage("ZZip")
        if zzip:
            self.infoUsage(zzip)
            self.mArgs.append("-DZZip_INCLUDE_DIR:PATH=" + zzip.getInclude())
            self.mArgs.append("-DZZip_LIBRARY_DBG:FILEPATH=" + zzip.getLibraryDebug())
            self.mArgs.append("-DZZip_LIBRARY_REL:FILEPATH=" + zzip.getLibraryRelease())


    # Create package.
    def createPackage(self, debug, release):
        return False


    # Build.
    def build(self, debug, release):
        res =  self.runCMake(
            debug, release,
            self.mArgs, self.mArgsD, self.mArgsR,
            self.mMakeArgs
        )
        if not sdkRoot.Platform.isMacOsx():
            self.updatePkgconfigInfo()
        return res


    # Download.
    def download(self):
        git = Git(self.mName, self.mPrefix, self.mVersion, self.mUrl, self.mRevision)
        return git.cloning(self.mPathSource)

