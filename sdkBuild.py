# ************************************************************
# T.Sang Tran <sang@augmenti.no>
# Augmenti (C) 2016 - Present
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ************************************************************
# TIPS!
#   *   Boolean is always TRUE for non-zero integer value.
#
# ------------------------------------------------------------
# - Import Packages
# ------------------------------------------------------------
import importlib

import platform
import subprocess
import sys

import argparse
import datetime
import os
import pathlib
import tarfile

# Local packages.
from System import *
from System.Core import *
from System.Library import *
from System.Util import *
from System.Program import *






# ------------------------------------------------------------
# - SDK Setup
# ------------------------------------------------------------
print("========================================")
print("= SDK Setup")
print("========================================")
print("Python Version:", sys.version)
if sys.version_info.major < 3:
    print("========================================")
    print("Required Python version greater than 3.")
    print("========================================")
    exit()

# Initilaise platform check.
sdkRoot.Platform = Platform()




# ------------------------------------------------------------
# - Arguments
# ------------------------------------------------------------
# ====================
# Core
# ====================
cmdParser = argparse.ArgumentParser(description="Script for creating a SDK.\r\nValues meaning (0) => Disabled  (1) => Enabled")
cmdParser.add_argument("--suffix", help="Compiler suffix. Ex: gcc4.8.2-x64, mscv2013-x86.", default="")
cmdParser.add_argument("--build-dir", help="The bulding directory.", default="")
cmdParser.add_argument("--sdk-dir", help="The SDK directory, contains compiled libraries.", default="")
cmdParser.add_argument("--source-dir", help="The Source directory, contains the source of libraries.", default="")
cmdParser.add_argument("--create-archives", help="Create archive files of custom configurations.", type=int, default=1)
cmdParser.add_argument("--create-sdk", help="Preform the SDK generation.", type=int, default=0)
cmdParser.add_argument("--enable-debug", help="Enable debug build.", type=int, default=1)
cmdParser.add_argument("--enable-release", help="Enable release build.", type=int, default=1)
cmdParser.add_argument("--info-setup", help="Information about setup.", type=int, default=0)


# ====================
# Optional
# ====================
cmdParser.add_argument("--download-sdk", help="Download system SDK.", type=int, default=0)
cmdParser.add_argument("--update", help="Update generated (Pkgconfig|SDK CMake) files.", type=int, default=0)
cmdParser.add_argument("--skip-build", help="Skip the building process.", default=0)
cmdParser.add_argument("--create-sdk-qmake", help="Create a SDK QMake file.", default=1)


# ====================
# Libraries
# ====================
cmdParser.add_argument("--build-all", help="Build all libraries.", type=int, default=0)
#cmdParser.add_argument("--build-boost", help="Build Boost library.", type=int, default=0)
#cmdParser.add_argument("--build-cairo", help="Build Cairo library.", type=int, default=0)
#cmdParser.add_argument("--build-expat", help="Build Expat XML library.", type=int, default=0)
#cmdParser.add_argument("--build-fontconfig", help="Build Fontconfig library.", type=int, default=0)
cmdParser.add_argument("--build-freeimage", help="Build FreeImage library.", type=int, default=0)
cmdParser.add_argument("--build-freetype", help="Build FreeType library.", type=int, default=0)
#cmdParser.add_argument("--build-geographic", help="Build Geographic library.", type=int, default=0)
#cmdParser.add_argument("--build-mygui", help="Build MyGUI library.", type=int, default=0)
cmdParser.add_argument("--build-jsoncpp", help="Build Jsoncpp library.", type=int, default=0)
#cmdParser.add_argument("--build-log4cplus", help="Build Log4cplus library.", type=int, default=0)
#cmdParser.add_argument("--build-lz4", help="Build LZ4 library.", type=int, default=0)
cmdParser.add_argument("--build-ogre", help="Build OGRE library.", type=int, default=0)
cmdParser.add_argument("--build-ogredeps", help="Build OGRE dependencies.", type=int, default=0)
cmdParser.add_argument("--build-ogrenext", help="Build OGRE next generation (ver >= 2.0).", type=int, default=0)
cmdParser.add_argument("--build-pixman", help="Build Pixman library.", type=int, default=0)
cmdParser.add_argument("--build-png", help="Build PNG library.", type=int, default=0)
#cmdParser.add_argument("--build-proj", help="Build PROJ library.", type=int, default=0)
#cmdParser.add_argument("--build-sdl", help="Build SDL library.", type=int, default=0)
#cmdParser.add_argument("--build-tinyxml2", help="Build TinyXML2 library.", type=int, default=0)
#cmdParser.add_argument("--build-xml2", help="Build XML2 library.", type=int, default=0)
cmdParser.add_argument("--build-zlib", help="Build zlib library.", type=int, default=0)
cmdParser.add_argument("--build-zzip", help="Build ZZip library.", type=int, default=0)
cmdParser.add_argument("--system-zlib", help="Use system version of zlib library.", type=int, default=0)


# ====================
# Unix
# ====================
# Package.
if sdkRoot.Platform.isUnix():
    cmdParser.add_argument("--create-packages", help="Create binary packages.", type=int, default=0)
    cmdParser.add_argument("--library-dir", help="Directory where files will be installed from the library.", default="usr")
    cmdParser.add_argument("--library-maintainer", help="Name of maintainer.", default="")
    cmdParser.add_argument("--library-maintainer-email", help="Email of maintainer.", default="")
    cmdParser.add_argument("--library-arch", help="Architecture (i386, amd64, all).", default="all")
    cmdParser.add_argument("--library-prefix", help="Prefix for library name.", default="sdk")

# Parse arguments.
cmdArgs = cmdParser.parse_args()




# ------------------------------------------------------------
# - Verify Arguments
# ------------------------------------------------------------
missingArgs = []
print("Verify arguments....")

# ====================
# Core
# ====================
if not cmdArgs.source_dir:
    missingArgs.append("Missing [--source-dir]")
if not cmdArgs.build_dir:
    missingArgs.append("Missing [--build-dir]")
if not cmdArgs.suffix:
    missingArgs.append("Missing [--suffix]")


# ====================
# Unix
# ====================
if sdkRoot.Platform.isUnix():
    if cmdArgs.create_packages:
        if not cmdArgs.package_maintainer:
            missingArgs.append("Missing [--library-maintainer]")
        if not cmdArgs.package_maintainer_email:
            missingArgs.append("Missing [--library-maintainer-emai]")
        if not cmdArgs.package_dir:
            missingArgs.append("Missing [--library-dir]")
        if not cmdArgs.package_arch in ("i368", "amd64", "all"):
            missingArgs.append("Invalid arch. Valid values: i386, amd64, all")

# Check for required arguments.
if len(missingArgs) > 0:
    print("------------------")
    print("Missing arguments")
    print("------------------")
    for i in missingArgs:
        print(" *", i)
    print("------------------")
    print("")
    cmdParser.print_help()
    exit()




# ------------------------------------------------------------
# - Verify Directories
# ------------------------------------------------------------
print("Verify directories.... ")

# Assign SDK path.
sdkRoot.InfoPath = InfoPath()
sdkRoot.InfoPath.mSuffix  = cmdArgs.suffix
if cmdArgs.sdk_dir:
    sdkRoot.InfoPath.mSdkRoot = os.path.abspath(cmdArgs.sdk_dir)

# Check for environment if not supplied.
if not sdkRoot.InfoPath.mSdkRoot:
    printChecking("SDK")
    sdkRoot.InfoPath.mSdkRoot = checkPathEnv("SDK_ROOT", "")
    if not sdkRoot.InfoPath.mSdkRoot:
        printChecking("SDK", "missing")
        print("----------------------")
        print("Missing SDK directory")
        print("----------------------")
        print("")
        cmdParser.print_help()
        exit()
    else:
        printChecking("SDK", "found at [" + sdkRoot.InfoPath.mSdkRoot + "]")


# Assign paths.
sdkRoot.InfoPath.mSdkRoot   = os.path.join(sdkRoot.InfoPath.mSdkRoot, sdkRoot.InfoPath.mSuffix)
sdkRoot.InfoPath.mSdkLib    = os.path.join(sdkRoot.InfoPath.mSdkRoot, "Public")
sdkRoot.InfoPath.mSource    = os.path.abspath(cmdArgs.source_dir)
sdkRoot.InfoPath.mBuild     = os.path.join(os.path.abspath(cmdArgs.build_dir), "Libraries")
sdkRoot.InfoPath.mArchive   = os.path.join(sdkRoot.InfoPath.mSource, "Archives")
sdkRoot.InfoPath.mCMake     = os.path.join(sdkRoot.InfoPath.mSdkRoot, "CMake")
sdkRoot.InfoPath.mPackage   = os.path.join(sdkRoot.InfoPath.mSdkRoot, "Packages")
sdkRoot.InfoPath.mPkgconfig         = os.path.join(sdkRoot.InfoPath.mSdkRoot, "Pkgconfig")
sdkRoot.InfoPath.mPkgconfigDebug    = os.path.join(sdkRoot.InfoPath.mPkgconfig, "debug")
sdkRoot.InfoPath.mPkgconfigRelease  = os.path.join(sdkRoot.InfoPath.mPkgconfig, "release")

# Create directories.
createDirectory("SDK (Root)", sdkRoot.InfoPath.mSdkRoot)
createDirectory("SDK (Library)", sdkRoot.InfoPath.mSdkLib)
createDirectory("build", sdkRoot.InfoPath.mBuild)
createDirectory("source", sdkRoot.InfoPath.mSource)
createDirectory("archive", sdkRoot.InfoPath.mArchive)
createDirectory("pkgconfig", sdkRoot.InfoPath.mPkgconfig)
createDirectory("cmake", sdkRoot.InfoPath.mCMake)
createDirectory("packages", sdkRoot.InfoPath.mPackage)

# Create log.
sdkRoot.Logger = LogFile(sdkRoot.InfoPath.mBuild, "sdkSetupBuild.log")
logMessage("========================================")
logMessage(("= Setup SDK (" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ")"))
logMessage("========================================")
logMessage(sdkRoot.InfoPath.toString())




# ------------------------------------------------------------
# - Verify System & Program
# ------------------------------------------------------------
logMessage("Checking platform....")
logMessage(sdkRoot.Platform.toString())

if sdkRoot.Platform.isUnix():
    logMessage("** Supporting Unix/Linux platform **")
elif sdkRoot.Platform.isWindows():
    logMessage("** Supporting Windows platform **")
elif sdkRoot.Platform.isDarwin():
    logMessage("** Supporting Apple platform **")
else:
    logMessage("** Unsupported platform")
    logMessage("Exiting....")
    exit()

logMessage("Checking Operating System....")
if sdkRoot.Platform.isUnknownSystem():
    logMessage("** Unsupported Operating System **")
    logMessage("Exiting....")
    exit()
else:
    logMessage("** Supported generic operating system **")

if sdkRoot.Platform.isUnix():
    if cmdArgs.create_packages:
        logMessage("Enable creating packages.")
        sdkRoot.InfoPackage = InfoPackage()
        sdkRoot.InfoPackage.mInstallDir = os.path.abspath(cmdArgs.package_dir)
        sdkRoot.InfoPackage.mMaintainerName = os.path.abspath(cmdArgs.package_maintainer)
        sdkRoot.InfoPackage.mMaintainerEmail = os.path.abspath(cmdArgs.package_maintainer_email)
        sdkRoot.InfoPackage.mArch = os.path.abspath(cmdArgs.package_arch)
        sdkRoot.InfoPackage.mPrefix = os.path.abspath(cmdArgs.package_prefix)
        logMessage(sdkRoot.InfoPackage.toString())


logMessage("========================================")
logMessage("= CHECKING PROGRAMS")
logMessage("========================================")
logMessage("Checking necessary programs....")

# Add program to check.
verifyProgs = True
checkProgs = [
    ProgramMake(),
    ProgramCMake(),
    ProgramMercurial(),
    ProgramGit(),
    ProgramWget()
]
if sdkRoot.Platform.isUnixAlike():
    checkProgs.append(ProgramAutomake())
    checkProgs.append(ProgramLibtool())
    checkProgs.append(ProgramPkgconfig())


if not cmdArgs.info_setup:
    # Set checked state
    for prog in checkProgs:
        if not prog.exists():
            verifyProgs = False

    if not verifyProgs:
        logMessage("----------------------------------------")
        logMessage("Missing required programs....")
        logMessage("Please follow the installation instructions:")
        for prog in checkProgs:
            if not prog.mFound:
                logMessage(("--", prog.mName, "--"))
                if prog.mWebsite:
                    logMessage(("    *", prog.mWebsite))
                msg = prog.help()
                if type(msg) is list:
                    for m in msg:
                        logMessage(("    *", m))
                else:
                    logMessage(("    *", msg))
        logMessage("----------------------------------------")
        exit()




# ------------------------------------------------------------
# - Prerequsites
# ------------------------------------------------------------
logMessage("========================================")
logMessage("= PREREQUISITES")
logMessage("========================================")
logMessage("List of system SDK requirements....")
for l in displayDevEnvironment(checkProgs):
    logMessage(l)

# Quit if the user just want to view the setup information.
if cmdArgs.info_setup:
    exit()

if sdkRoot.Platform.isWindows():
    logMessage(" * DirectX (https://www.microsoft.com/en-us/download/details.aspx?id=6812)")
    logMessage(" * OpenAL (https://www.openal.org/downloads/OpenAL11CoreSDK.zip)")
    if cmdArgs.download_sdk:
        download = input("Du you want to download these files? (Y) or (N):")
        if download in ("y", "Y"):
            wget = Wget("OpenAL11CoreSDK.zip", "https://www.openal.org/downloads/OpenAL11CoreSDK.zip")
            wget.download(sdkRoot.InfoPath.mArchive)

            logDownloading("DirectX")
            logMessage("Manually download DirectX SDK: https://www.microsoft.com/en-us/download/details.aspx?id=6812")

            print("Please install these packages first. Then run the script again with \"--download-sdk\" flag disabled.")
            exit()
else:
    if sdkRoot.Platform.isUnixAlike():
        if sdkRoot.Platform.isDarwin():
            bashrc = ".bash_profile"
        else:
            bashrc = ".bashrc"
        logMessage("----------------------------------------")
        logMessage("- Recommended Actions")
        logMessage("----------------------------------------")
        logMessage("Recommended actions after completation:")
        logMessage(" * Set PKG_CONFIG_PATH environment to " + sdkRoot.InfoPath.mPkgconfig)
        logMessage(" * Set SDK_ROOT environment to " + sdkRoot.InfoPath.mSdkRoot)
        logMessage("")
        logMessage("Example:")
        logMessage(" * [HOME_DIR]/" + bashrc)
        logMessage("   In " + bashrc +", at the end of file, enter following:")
        logMessage("   export PKG_CONFIG_PATH=" + sdkRoot.InfoPath.mPkgconfig)
        logMessage("   export SDK_ROOT=" + sdkRoot.InfoPath.mSdkRoot)
        logMessage("")
        if sdkRoot.Platform.isUnix():
            logMessage(" * /etc/enviroment")
            logMessage("   PKG_CONFIG_PATH=" + sdkRoot.InfoPath.mPkgconfig)
            logMessage("   SDK_ROOT=" + sdkRoot.InfoPath.mSdkRoot)
            logMessage("")
        logMessage("----------------------------------------")
        logMessage("- Note")
        logMessage("----------------------------------------")
        logMessage(" * If you specified in " + bashrc + ", you have to remember to start, for instance,")
        logMessage("   cmake-gui in a terminal, otherwise cmake-gui could not locate the SDK_ROOT.")
        if sdkRoot.Platform.isUnix():
            logMessage(" * If you specified in /etc/environment, for instance, cmake-gui could")
            logMessage("   locate SDK_ROOT.")

print("")
print("--------------------------------------------------------------------")
print(" -- Please make sure you have met the requirement before continue --")
print("--------------------------------------------------------------------")
answer = input("Du you want to continue the process? (Y) or (N): ")
if not answer in ("y", "Y"):
    exit()


# Set PKG config environment for local usage.
if os.getenv("PKG_CONFIG_PATH", "") == "":
    logMessage("Setup pkg-config for local usage....")
    os.environ["PKG_CONFIG_PATH"] = sdkRoot.InfoPath.mPkgconfig
    logMessage("Set local PKG_CONFIG_PATH to " + sdkRoot.InfoPath.mPkgconfig)




# ------------------------------------------------------------
# - Build Archives
# ------------------------------------------------------------
if cmdArgs.create_archives:
    logMessage("========================================")
    logMessage("= BUILD ARCHIVES")
    logMessage("========================================")

    # Display current working directory. Assumes it is where the script is beeing called.
    root = os.path.join(os.getcwd(), "Sources")
    logMessage("Looking files in [" + root + "]....")

    # Looping prefix directory.
    for prefix in os.listdir(root):
        # Looping version directory.
        pathPrefix = os.path.join(root, prefix)
        if os.path.isdir(pathPrefix):
            logMessage("[" + prefix + "]")
            for version in os.listdir(pathPrefix):

                # Process only if is a directory, this will skip adding existing archive files.
                pathVersion = os.path.join(pathPrefix, version)
                if os.path.isdir(pathVersion):

                    # Add into archive.
                    logMessage("  * Processing version: " + version)
                    tar = tarfile.open(pathVersion + ".tbz2", "w:bz2")
                    for file in os.listdir(pathVersion):
                        curFile = os.path.join(pathVersion, file)
                        if os.path.isfile(curFile):
                            tar.add(curFile, arcname=os.path.basename(curFile), recursive=False)
                        if os.path.isdir(curFile):
                            tar.add(curFile, arcname=os.path.basename(curFile))
                    tar.close()




# ------------------------------------------------------------
# - Download & Build
# ------------------------------------------------------------
logMessage("========================================")
logMessage("= PROCESSING BUILD SDK")
logMessage("========================================")


checkLibraries = [
    [cmdArgs.build_all or cmdArgs.build_zlib, LibraryZlib(cmdArgs.system_zlib)],
    [cmdArgs.build_all or cmdArgs.build_zzip, LibraryZZip()],
    [cmdArgs.build_all or cmdArgs.build_freeimage, LibraryFreeImage()],
    [cmdArgs.build_all or cmdArgs.build_freetype, LibraryFreeType()],
    [cmdArgs.build_all or cmdArgs.build_ogredeps, LibraryOgreDeps()],
    [cmdArgs.build_all or cmdArgs.build_ogre, LibraryOgre(cmdArgs.build_ogrenext)],
    [cmdArgs.build_all or cmdArgs.build_jsoncpp, LibraryJsoncpp()],
    [cmdArgs.build_all or cmdArgs.build_png, LibraryLibpng()],
    [cmdArgs.build_all or cmdArgs.build_pixman, LibraryPixman()]
]

if cmdArgs.create_sdk:
    logMessage("----------------------------------------")
    logMessage("- Download && Build Sources")
    logMessage("----------------------------------------")

    # Create SDK, README files and etc.
    sdkFile     = IncludeSdkFile()
    readMeFile  = ReadMeFile(sdkRoot.Platform.toString())
    qmakeFile   = QMakeSdkFile()
    exportFile  = ExportSdkFile()

    # Process packages.
    sdkTimer = Timer()
    for pkg in checkLibraries:
        # Working vars.
        enabled = pkg[0]
        library = pkg[1]

        # Skip building process.
        if cmdArgs.skip_build:
            logMessage("** SKIP ** building " + library.mName + ".")
            continue

        if enabled:
            library.header()
            if library.download():
                # Start timer.
                timer = Timer()

                # Building process.
                if library.build(cmdArgs.enable_debug, cmdArgs.enable_release):
                    library.saveSdkFiles()

                # Log time elapse.
                logMessage("Build Time: " + timer.toString())
            library.footer()

        # Create CMake, README files and etc.
        if library.existLibrary():
            if library.mPrecompiled or library.mSystemUsage:
                if library.mSystemInfo:
                    sdkFile.addFullPath(library.mCMakeName, library.mPathSystem.mSdkRoot)
            else:
                sdkFile.add(library.mCMakeName, library.mPrefix, library.mVersion)
                readMeFile.add(library.mReadMe)

            qmakeFile.add(library, cmdArgs.create_sdk_qmake)
            exportFile.add(library)

    if not cmdArgs.skip_build:
        logMessage("SDK Build Time: " + sdkTimer.toString())

    sdkFile.save()
    readMeFile.save()
    qmakeFile.save()
    exportFile.save()

    # List of the results.
    logMessage("========================================")
    logMessage("= RESULTS")
    logMessage("========================================")

    # For pretty output, we find the max length.
    maxLen = 0
    for pkg in checkLibraries:
        # Working vars.
        enabled = pkg[0]
        library = pkg[1]

        if enabled and not cmdArgs.skip_build:
            nameLen = len(library.mName)
            if nameLen > maxLen:
                maxLen = nameLen

    f = '{0:>%d}\tDebug: {1:>10}\tRelease: {2}' % (maxLen)
    for pkg in checkLibraries:
        # Working vars.
        enabled = pkg[0]
        library = pkg[1]

        if enabled and not cmdArgs.skip_build:
            if not library.mBuildStatusDebug and not library.mBuildStatusRelease:
                statusDebug   = "DISABLED"
                statusRelease = "DISABLED"
                if cmdArgs.enable_debug:
                    if library.existLibraryDebug():
                        statusDebug = "COMPLETED"
                    else:
                        statusDebug = "FAILED"

                if cmdArgs.enable_release:
                    if library.existLibraryRelease():
                        statusRelease = "COMPLETED"
                    else:
                        statusRelease = "FAILED"

                logMessage(
                    f.format(library.mName,
                    statusDebug,
                    statusRelease)
                )
            else:
                logMessage(
                    f.format(library.mName,
                    library.mBuildStatusDebug,
                    library.mBuildStatusRelease)
                )



sdkRoot.Logger.close()
exit()
# ------------------------------------------------------------
# - Binary Packages
# ------------------------------------------------------------
if cmdArgs.create_packages:
    print()
    logMessage("----------------------------------------")
    logMessage("- Build Binary Packages")
    logMessage("----------------------------------------")
    sdkUtils.logResult("----------------------------------------")
    sdkUtils.logResult("- Build Binary Packages")
    sdkUtils.logResult("----------------------------------------")
    logMessage("Maintainer name:   " + sdkGlobals.PackageMaintainer)
    logMessage("Maintainer email:  " + sdkGlobals.PackageMaintainerEmail)
    logMessage("Install directory: " + sdkGlobals.PackageInstallDir)
    logMessage("Architecture:      " + sdkGlobals.PackageArch)
    logMessage("Package Prefix:    " + sdkGlobals.PackagePrefix)

    for pkg in checkLibraries:
        # Working vars.
        enabled = pkg[0]
        library = pkg[1]

        if enabled:
            library.header()
            library.createPackage(cmdArgs.enable_debug, cmdArgs.enable_release)
            library.footer()


sdkRoot.Logger.close()
exit()
