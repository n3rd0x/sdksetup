import datetime
import subprocess
import sys
import os

class Base():
    def __init__(self, url):
        self.url = url
        print(self.url)
        
        
    def chekcProgram(self):
        print("---------------------------")
        process = subprocess.Popen(["which", "test3"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = process.communicate()[0]
        print("Out:", out)
        print("Return: ", process.returncode)
        
        
        print("---------------------------")
        process = subprocess.Popen(["which", "test2"])
        out = process.communicate()[0]
        print("Out:", out)
        print("Return: ", process.returncode)
        
        
        print("---------------------------")
        process = subprocess.Popen(["which", "git2s"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        if err:
            print("Error")
        else:
            print("Good")
        print("Return: ", process.returncode)
        
        print("RawOut: ", out)
        print("RawErr: ", err)
        #print("DecOut: ", out.decode(encoding="UTF-8"))
        #print("DecErr: ", err.decode(encoding="UTF-8"))
        print("Type out:", type(out))
        print("Type rr:", type(err))
        
        if type(out) is bytes:
            print("IS BYTE")
       
        
        file = open("test.log", 'w')
        #file.write(str(out.decode(encoding="UTF-8")) + "\r\n")
        #file.write(str(err.decode(encoding="UTF-8")) + "\r\n")
        file.write(str(out) + "\r\n")
        file.write(str(err) + "\r\n")
        file.close()
        
        testing = "/usr/share"
        tes = "/lig"
        path = os.path.abspath(os.path.join(tes, testing))
        print(path)

        
    def sudo(self):
        process = subprocess.Popen(["sudo", "apt-get", "update"])
        process.wait()
        if process.returncode >= 1:
            print("Failed to sudo")
        else:
            print("Update repository completed")
        
    def overrideFunc(self):
        print("Base overrideFunc")
        
        
        
class Derived(Base):
    message = "None"
    def __init__(self, msg = "Default message"):
        self.message = msg
        Base.__init__(self, "Derived URL")
        print("Message:", self.message)
    
        
    def overrideFunc(self):
        print("Derived overrideFunc")
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(timestamp)
        
        
    def printMessage(self):
        print("Message:", self.message)
        
        
test = Derived("This is a passing argument.")
test.chekcProgram()
test.overrideFunc()
#test.sudo()
obj = Derived()
obj.printMessage()
        
